use regex::Regex;
use std::cmp;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Game {
    hands: Vec<Hand>,
}

impl Game {
    fn new(line: &str) -> Game {
        let hands: Vec<Hand> = line
            .split(": ")
            .collect::<Vec<_>>()
            .get(1)
            .unwrap()
            .split("; ")
            .map(|x| Hand::new(x))
            .collect();
        Game { hands }
    }

    fn possible(&self, max: &Hand) -> bool {
        for h in &self.hands {
            if h.gt(max) {
                return false;
            }
        }
        true
    }

    fn min_hand(&self) -> Hand {
        let mut red = 0;
        let mut blue = 0;
        let mut green = 0;
        for h in &self.hands {
            red = cmp::max(red, h.red);
            blue = cmp::max(blue, h.blue);
            green = cmp::max(green, h.green);
        }
        Hand { red, blue, green }
    }
}

#[derive(Debug)]
struct Hand {
    red: u32,
    green: u32,
    blue: u32,
}

impl Hand {
    fn new(line: &str) -> Hand {
        let rered = Regex::new(r"([0-9]+) red").unwrap();
        let reblue = Regex::new(r"([0-9]+) blue").unwrap();
        let regreen = Regex::new(r"([0-9]+) green").unwrap();
        let red = rered.captures(line).map_or(0, |cap| {
            cap.get(1).map_or(0, |x| x.as_str().parse().unwrap())
        });
        let green = regreen.captures(line).map_or(0, |cap| {
            cap.get(1).map_or(0, |x| x.as_str().parse().unwrap())
        });
        let blue = reblue.captures(line).map_or(0, |cap| {
            cap.get(1).map_or(0, |x| x.as_str().parse().unwrap())
        });
        Hand { red, green, blue }
    }

    fn gt(&self, other: &Hand) -> bool {
        self.red > other.red || self.green > other.green || self.blue > other.blue
    }

    fn power(&self) -> u32 {
        self.red * self.blue * self.green
    }
}

fn part1(contents: &str) {
    let games: Vec<Game> = contents.lines().map(|l| Game::new(l)).collect();
    let max = Hand {
        red: 12,
        green: 13,
        blue: 14,
    };
    let mut total = 0;
    for (i, g) in games.iter().enumerate() {
        if g.possible(&max) {
            total += i + 1;
        }
    }
    println!("Part 1: {:?}", total);
}

fn part2(contents: &str) {
    let games: Vec<Game> = contents.lines().map(|l| Game::new(l)).collect();
    let mut total = 0;
    for g in games {
        total += g.min_hand().power();
    }
    println!("Part 2: {:?}", total);
}
