use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn extract(line: &str) -> u32 {
    let mut first: Option<u32> = None;
    let mut last = 0;
    for c in line.chars() {
        if let Some(x) = c.to_digit(10) {
            match first {
                None => first = Some(x),
                Some(_) => (),
            }
            last = x;
        }
    }
    if let Some(f) = first {
        return 10 * f + last;
    }
    return last;
}

fn starting_digit(line: &str) -> Option<u32> {
    let digits: HashMap<&str, u32> = HashMap::from([
        ("0", 0),
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
        ("6", 6),
        ("7", 7),
        ("8", 8),
        ("9", 9),
        ("zero", 0),
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ]);
    for key in digits.keys() {
        if line.starts_with(key) {
            return digits.get(key).copied();
        }
    }
    return None;
}

fn extract2(line: &str) -> u32 {
    let mut first: Option<u32> = None;
    let mut last = 0;
    for i in 0..line.len() {
        if let Some(x) = starting_digit(&line[i..]) {
            match first {
                None => first = Some(x),
                Some(_) => (),
            }
            last = x;
        }
    }
    if let Some(f) = first {
        return 10 * f + last;
    }
    return last;
}

fn part1(contents: &str) {
    let mut total = 0;
    for line in contents.lines() {
        total += extract(line);
    }
    println!("Part 1: Calibration value: {:?}", total);
}

fn part2(contents: &str) {
    let mut total = 0;
    for line in contents.lines() {
        total += extract2(line);
    }
    println!("Part 1: Calibration value: {:?}", total);
}
