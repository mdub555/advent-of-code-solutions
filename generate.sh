#!/usr/bin/env sh

if [ $# -ne 2 ]; then
  echo "Usage:"
  echo "generate.sh <year> <language>"
  exit 1
fi

year=$1
lang=$2

if [ -d "$year" ]; then
  echo "File \"$year\" already exists. Delete it if you want to regenerate." & exit 2
fi

for day in {01..25}; do
  dir="$year/day$day"
  mkdir -p "$dir"
  cp -r "templates/$lang"/* "$dir"
done
