#!/usr/bin/env python3

import sys

from dataclasses import dataclass

@dataclass
class Point:
    row: int
    col: int

    @classmethod
    def from_list(cls, l: list):
        return cls(row = int(l[1]), col = int(l[0]))

@dataclass
class Fold:
    axis: str
    pos: int

    @classmethod
    def from_list(cls, l: list):
        return cls(axis = l[0], pos = int(l[1]))

class Grid:
    def __init__(self):
        self.grid = [[False]]

    def add(self, point: Point):
        self._allocate(point.row, point.col)
        self.grid[point.row][point.col] = True

    def fold(self, fold: Fold):
        if fold.axis == 'x':
            self._fold_left(fold.pos)
        else:
            self._fold_up(fold.pos)

    def _fold_up(self, pos: int):
        if pos < len(self.grid) // 2:
            pass
        for row in range(pos+1, len(self.grid)):
            for col in range(len(self.grid[row])):
                if self.grid[row][col]:
                    self.grid[(pos<<1) - row][col] = True
        self.grid = self.grid[:pos]

    def _fold_left(self, pos: int):
        if pos < len(self.grid[0]) // 2:
            pass
        for row in range(len(self.grid)):
            for col in range(pos+1, len(self.grid[0])):
                if self.grid[row][col]:
                    self.grid[row][(pos<<1) - col] = True
        for row in range(len(self.grid)):
            self.grid[row] = self.grid[row][:pos]

    def _allocate(self, row, col):
        if col > len(self.grid[0]) - 1:
            width = len(self.grid[0])
            for i in range(len(self.grid)):
                self.grid[i] += [False for _ in range(col-width+1)]
        if row > len(self.grid) - 1:
            self.grid += [[False for _ in range(len(self.grid[0]))]
                    for _ in range(row - len(self.grid)+1)]

    def __str__(self):
        output = ""
        for row in self.grid:
            for cell in row:
                if cell:
                    output += "#"
                else:
                    output += "."
            output += "\n"
        return output

def part1(points, folds):
    g = Grid()
    for point in points:
        g.add(point)
    g.fold(folds[0])
    total = 0
    for row in g.grid:
        for cell in row:
            if cell:
                total += 1
    return total

def part2(points, folds):
    g = Grid()
    for point in points:
        g.add(point)
    for fold in folds:
        g.fold(fold)
    return str(g)

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]
point_lines = lines[:lines.index("")]
fold_lines = lines[lines.index("")+1:]
points = [Point.from_list(p.split(',')) for p in point_lines]
folds = [Fold.from_list(f.split(' ')[2].split('=')) for f in fold_lines]

print("part 1: %i points after one fold" % part1(points, folds))
print("part 2: output is\n%s" % part2(points, folds))
