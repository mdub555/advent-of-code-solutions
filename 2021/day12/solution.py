#!/usr/bin/env python3
import sys

from copy import deepcopy
from dataclasses import dataclass, field

@dataclass
class Node:
    name: str
    is_big: bool
    neighbors: set[str] = field(default_factory=set)
    visited: bool = field(default=False)

def reset(graph):
    for node in graph:
        graph[node].visited = False

#     start
#     /   \
# c--A-----b--d
#     \   /
#      end
# start,A,b,A,c,A,end
# start,A,b,A,end
# start,A,b,end
# start,A,c,A,b,A,end
# start,A,c,A,b,end
# start,A,c,A,end
# start,A,end
# start,b,A,c,A,end
# start,b,A,end
# start,b,end

def visit(graph, node, path):
    path.append(node)
    if node == "end":
        return [path]
    all_paths = []
    for neighbor in graph[node].neighbors:
        if graph[neighbor].is_big or neighbor not in path:
            paths = visit(graph, neighbor, deepcopy(path))
            for p in paths:
                if len(p) > 0:
                    all_paths.append(p)
    return all_paths

def all_paths(graph):
    all_paths = []
    for neighbor in graph["start"].neighbors:
        paths = visit(graph, neighbor, ["start"])
        for path in paths:
            all_paths.append(path)
    return all_paths

def part1(graph):
    return len(all_paths(graph))

def visit2(graph, node, path, twice):
    path.append(node)
    if node == "end":
        return [path]
    all_paths = []
    for neighbor in graph[node].neighbors:
        paths = []
        if neighbor == "start":
            continue
        if graph[neighbor].is_big:
            paths = visit2(graph, neighbor, deepcopy(path), twice)
        elif neighbor not in path:
            paths = visit2(graph, neighbor, deepcopy(path), twice)
        elif not twice:
            paths = visit2(graph, neighbor, deepcopy(path), True)
        for p in paths:
            if len(p) > 0:
                all_paths.append(p)
    return all_paths

def all_paths2(graph):
    all_paths = []
    for neighbor in graph["start"].neighbors:
        paths = visit2(graph, neighbor, ["start"], False)
        for path in paths:
            all_paths.append(path)
    return all_paths

def part2(graph):
    paths = all_paths2(graph)
    return len(paths)

graph = dict()
file_name = sys.argv[1]
with open(file_name) as f:
    for line in f:
        nodes = line.strip().split('-')
        for node in nodes:
            if node not in graph:
                graph[node] = Node(node, node.isupper())
        graph[nodes[0]].neighbors.add(nodes[1])
        graph[nodes[1]].neighbors.add(nodes[0])

print("part 1: %i paths" % part1(graph))
print("part 2: %i paths" % part2(graph))
