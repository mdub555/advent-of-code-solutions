#!/usr/bin/env python3

import sys

file_name = sys.argv[1]
diagnostics = []
bitwidth = 0
with open(file_name) as f:
    for line in f:
        line = line.strip()
        bitwidth = len(line)
        diagnostics.append(int(line, 2))

def most_common(diagnostics, bitwidth):
    most = 0
    for i in range(bitwidth):
        total = 0
        for line in diagnostics:
            if line & (1<<(bitwidth-i-1)):
                total += 1
            if total<<1 >= len(diagnostics):
                most |= 1<<(bitwidth-i-1)
                break
    return most

def least_common(diagnostics, bitwidth):
    return most_common(diagnostics, bitwidth) ^ ((1<<bitwidth)-1)

def part1(diagnostics, bitwidth):
    epsilon = most_common(diagnostics, bitwidth)
    omega = least_common(diagnostics, bitwidth)
    power = epsilon * omega
    return power

def filterfn(line, index, compareTo, bitwidth):
    desiredBit = (compareTo >> (bitwidth - index - 1)) & 1
    actualBit = (line >> (bitwidth - index - 1)) & 1
    return not bool(desiredBit ^ actualBit)

def most_common_after_filter(diagnostics, bitwidth):
    array = diagnostics
    index = 0
    while len(array) > 1 and index < bitwidth:
        most = most_common(array, bitwidth)
        array = list(filter(lambda x: filterfn(x, index, most, bitwidth), array))
        index += 1
    return array[0]

def least_common_after_filter(diagnostics, bitwidth):
    array = diagnostics
    index = 0
    while len(array) > 1 and index < bitwidth:
        least = least_common(array, bitwidth)
        array = list(filter(lambda x: filterfn(x, index, least, bitwidth), array))
        index += 1
    return array[0]

def part2(diagnostics, bitwidth):
    oxygen_generator_rating = most_common_after_filter(diagnostics, bitwidth)
    c02_scrubber_rating = least_common_after_filter(diagnostics, bitwidth)
    life_support_rating = oxygen_generator_rating * c02_scrubber_rating
    return life_support_rating

print("part 1: power = %i" % part1(diagnostics, bitwidth))
print("part 2: life support rating = %s" % part2(diagnostics, bitwidth))
