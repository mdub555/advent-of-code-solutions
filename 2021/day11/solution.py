#!/usr/bin/env python3

import sys

from copy import deepcopy

def flash(grid, row, col):
    for ri in range(-1, 2):
        for ci in range(-1, 2):
            if ri == 0 and ci == 0:
                continue
            r = row + ri
            c = col + ci
            if r < 0 or r > len(grid)-1:
                continue
            if c < 0 or c > len(grid[0])-1:
                continue
            grid[r][c] += 1

def can_propagate(grid):
    for row in grid:
        for cell in row:
            if cell:
                return True
    return False

def count_flashes(grid):
    flashes = 0
    for row in grid:
        for cell in row:
            if not cell:
                flashes += 1
    return flashes

def all_flashed(grid):
    return count_flashes(grid) == len(grid) * len(grid[0])

def step(grid):
    rows = len(grid)
    cols = len(grid[0])
    propagate = [[[0 for _ in range(cols)]
                    for _ in range(rows)]
                    for _ in range(2)]
    for row in range(rows):
        for col in range(cols):
            grid[row][col] += 1
            if grid[row][col] > 9:
                flash(propagate[0], row, col)
                grid[row][col] = 0
    iteration = 0
    while can_propagate(propagate[iteration]):
        for row in range(rows):
            for col in range(cols):
                if grid[row][col] == 0:
                    propagate[iteration][row][col] = 0
                    continue
                grid[row][col] += propagate[iteration][row][col]
                if grid[row][col] > 9:
                    flash(propagate[(iteration+1)%2], row, col)
                    grid[row][col] = 0
                propagate[iteration][row][col] = 0
        iteration = (iteration + 1) % 2

def print_grid(grid):
    output = ""
    for row in grid:
        for cell in row:
            if not cell:
                output += '.'
            else:
                output += str(cell)
        output += "\n"
    print(output)

def part1(grid, steps):
    flashes = 0
    for i in range(steps):
        step(grid)
        flashes += count_flashes(grid)
    return flashes

def part2(grid):
    steps = 0
    while not all_flashed(grid):
        step(grid)
        steps += 1
    return steps

file_name = sys.argv[1]
with open(file_name) as f:
    grid1 = [[int(c) for c in l.strip()] for l in f.readlines()]
grid2 = deepcopy(grid1)

print("part 1: %i flashes in %i steps" % (part1(grid1, 100), 100))
print("part 2: all will flash in %i steps" % part2(grid2))
