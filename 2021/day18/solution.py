#!/usr/bin/env python3

import sys

from dataclasses import dataclass, field
from typing import Any

#               o
#           /       \
#          o         o
#        /    \     / \
#       o      o   1   1
#      / \    / \
#     o   4  7   o
#    / \        / \
#   o   4      o   9
#  / \        / \
# 4   3      8   4

#               o
#           /       \
#          o         o
#        /    \     / \
#       o      o   1   1
#      / \    / \
#     o   4  7   o
#    / \        / \
#   0   7      o   9
#             / \
#            8   4

#               o
#           /       \
#          o         o
#        /    \     / \
#       o      o   1   1
#      / \    / \
#     o   4  15  o
#    / \        / \
#   0   7      0  13

#                 o
#            /        \
#           o          o
#        /     \      / \
#       o       o    1   1
#      / \    /   \
#     o   4  o     o
#    / \    / \   / \
#   0   7  7   8 0  13

#                 o
#            /        \
#           o          o
#        /     \      / \
#       o       o    1   1
#      / \    /   \
#     o   4  o     o
#    / \    / \   / \
#   0   7  7   8 0   o
#                   / \
#                  6   7

#                 o
#            /        \
#           o          o
#        /     \      / \
#       o       o    8   1
#      / \    /   \
#     o   4  o     o
#    / \    / \   / \
#   0   7  7   8 6   o

@dataclass
class Node:
    left: Any = None
    right: Any = None
    data: Any = None

    def isleaf(self):
        return self.data is not None

    def magnitude(self):
        if self.isleaf():
            return self.data
        return 3 * self.left.magnitude() + 2 * self.right.magnitude()

    def __str__(self):
        if self.isleaf():
            return str(self.data)
        return "[" + str(self.left) + "," + str(self.right) + "]"

def all_leaves(root: Node):
    if root.isleaf():
        return [root]
    return all_leaves(root.left) + all_leaves(root.right)

def next_leaf(curr: Node, root: Node):
    leaves = all_leaves(root)
    for i in range(len(leaves)):
        if curr is leaves[i]:
            if i == len(leaves) - 1:
                return None
            return leaves[i + 1]

def prev_leaf(curr: Node, root: Node):
    leaves = all_leaves(root)
    for i in range(len(leaves)):
        if curr is leaves[i]:
            if i == 0:
                return None
            return leaves[i - 1]

def add(a: Node, b: Node) -> Node:
    return Node(left=a, right=b)

def explode(node: Node, root: Node):
    left = prev_leaf(node.left, root)
    if left:
        left.data += node.left.data
    right = next_leaf(node.right, root)
    if right:
        right.data += node.right.data
    node.left = None
    node.right = None
    node.data = 0

def explode_any(node: Node, root: Node, depth: int = 1):
    if depth > 4 and not node.isleaf():
        explode(node, root)
        return True
    if node.isleaf():
        return False
    exploded = explode_any(node.left, root, depth + 1)
    if exploded:
        return True
    return explode_any(node.right, root, depth + 1)

def split(node: Node):
    node.left = Node(data=node.data//2)
    node.right = Node(data=node.data//2+node.data%2)
    node.data = None

def split_any(node: Node):
    if node.isleaf() and node.data >= 10:
        split(node)
        return True
    if node.isleaf():
        return False
    splitted = split_any(node.left)
    if splitted:
        return True
    return split_any(node.right)

def reduce(root: Node):
    while True:
        if explode_any(root, root):
            continue
        if split_any(root):
            continue
        break

def from_line(line: str) -> Node:
    stack = []
    for c in line:
        if c == '[' or c == ',':
            continue
        elif c.isnumeric():
            stack.append(Node(data=int(c)))
        elif c == ']':
            right = stack.pop()
            left = stack.pop()
            stack.append(Node(left=left, right=right))
    return stack[0]

def part1(lines):
    start = from_line(lines[0])
    for i in range(1, len(lines)):
        tree = from_line(lines[i])
        start = add(start, tree)
        reduce(start)
    return start.magnitude()

def part2(lines):
    all_numbers = [from_line(l) for l in lines]
    max_magnitude = 0
    for i in range(len(lines)):
        for j in range(len(lines)):
            if i == j:
                continue
            num1 = from_line(lines[i])
            num2 = from_line(lines[j])
            addition = add(num1, num2)
            reduce(addition)
            if addition.magnitude() > max_magnitude:
                max_magnitude = addition.magnitude()
    return max_magnitude

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]

print("part 1: magnitude = %d" % part1(lines))
print("part 2: max magnitude = %d" % part2(lines))
