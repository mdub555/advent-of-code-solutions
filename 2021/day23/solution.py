#!/usr/bin/env python3

import copy
import math
import sys
import time

PLAYERS = ['A', 'B', 'C', 'D']
ROOMS = {3: 'A', 5: 'B', 7: 'C', 9: 'D'}
COSTS = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}

def getplayers(board):
    players = []
    for row in range(1, len(board)-1):
        for col in range(1, len(board[row])-1):
            if board[row][col] in PLAYERS:
                players.append((row, col))
    return players

def isvalidmove(board, move):
    player = board[move[0][0]][move[0][1]]
    fromroom = getroom(move[0])
    toroom = getroom(move[1])
    if toroom == 'H' and move[1][1] in ROOMS:
        # in doorway
        return False
    if fromroom == 'H' and toroom == 'H':
        # must move into a room from the hall
        return False
    if toroom != 'H' and player != toroom:
        # trying to move into room that's not theirs
        return False
    if toroom != 'H' and board[move[1][0]+1][move[1][1]] == '.':
        # must move as far down in a room as possible
        return False
    if toroom != 'H' and not allcorrectinroom(board, move[1]):
        # a player of the wrong type is in the room
        return False
    if fromroom == player and allcorrectinroom(board, move[0]):
        # trying to move from a solved position
        return False
    return True

def allcorrectinroom(board, pos):
    room = getroom(pos)
    for row in range(2, len(board)-1):
        if board[row][pos[1]] != '.' and board[row][pos[1]] != room:
            return False
    return True

def getallpositions(board, pos, visited=None):
    if visited is None:
        visited = set()
    adjacent = [(pos[0]+1, pos[1]), (pos[0]-1, pos[1]),
                (pos[0], pos[1]+1), (pos[0], pos[1]-1)]
    for p in adjacent:
        if p in visited:
            continue
        if board[p[0]][p[1]] == '.':
            visited.add(p)
            getallpositions(board, p, visited)
    return list(visited)

def _getvalidmoves(board, player):
    possiblepos = getallpositions(board, player)
    validmoves = []
    for pos in possiblepos:
        if isvalidmove(board, (player, pos)):
            validmoves.append((player, pos))
    return validmoves

def getvalidmoves(board):
    moves = []
    players = getplayers(board)
    for player in players:
        moves += _getvalidmoves(board, player)
    return moves

def getroom(pos):
    if pos[0] == 1:
        return 'H'
    return ROOMS[pos[1]]

def getcost(board, move):
    # move along the hallway
    horiz = abs(move[0][1]-move[1][1])
    # simulate moving to the hall if necessary
    vert = abs(move[0][0]-1) + abs(move[1][0]-1)
    return (horiz + vert) * COSTS[board[move[0][0]][move[0][1]]]

def domove(board, move):
    board[move[1][0]][move[1][1]] = board[move[0][0]][move[0][1]]
    board[move[0][0]][move[0][1]] = '.'

def boardstr(board):
    output = ""
    for row in board:
        output += "".join(row) + "\n"
    return output

def issolved(board):
    for player in getplayers(board):
        if getroom(player) != board[player[0]][player[1]]:
            return False
    return True

def minsolve(board, knowncosts = None):
    if issolved(board):
        return 0
    if knowncosts is None:
        knowncosts = {}
    if boardstr(board) in knowncosts:
        return knowncosts[boardstr(board)]
    mini = math.inf
    for move in getvalidmoves(board):
        newboard = copy.deepcopy(board)
        domove(newboard, move)
        cost = minsolve(newboard, knowncosts) + getcost(board, move)
        mini = min(cost, mini)
    knowncosts[boardstr(board)] = mini
    return mini

# the solution is the same for parts 1 and 2, just with a different input file
def part1(lines):
    board = [[c for c in line] for line in lines]
    return minsolve(board)

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip('\n') for l in f.readlines()]
    for i in range(3, len(lines)):
        lines[i] += "  "

print("partn: energy = {}".format(part1(lines)))
