#!/usr/bin/env python3

import sys

def cost1(pos, crabs):
    total = 0
    for crab in crabs:
        total += abs(crab - pos)
    return total

def cost2(pos, crabs):
    total = 0
    for crab in crabs:
        movement = abs(crab - pos)
        total += movement * (movement + 1) // 2
    return total

def part1(crabs):
    crabs.sort()
    best_cost = None
    best_pos = None
    for i in range(crabs[0], crabs[len(crabs)-1]+1):
        c = cost1(i, crabs)
        if best_cost is None or best_cost > c:
            best_cost = c
            best_pos = i
    return best_cost

def part2(crabs):
    crabs.sort()
    best_cost = None
    best_pos = None
    for i in range(crabs[0], crabs[len(crabs)-1]+1):
        c = cost2(i, crabs)
        if best_cost is None or best_cost > c:
            best_cost = c
            best_pos = i
    return best_cost

file_name = sys.argv[1]
with open(file_name) as f:
    for line in f:
        crabs = [int(x) for x in line.strip().split(',')]

print("part 1: cost of best pos: %i" % part1(crabs))
print("part 2: cost of best pos: %i" % part2(crabs))
