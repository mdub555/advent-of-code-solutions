#!/usr/bin/env python3

import sys

file_name = sys.argv[1]
readings = []
with open(file_name) as f:
    for line in f:
        readings.append(int(line.strip()))

def part1(readings):
    num_increases = 0
    for i in range(len(readings) - 1):
        if readings[i + 1] > readings[i]:
            num_increases += 1
    return num_increases

def part2(readings):
    num_increases = 0
    for i in range(len(readings) - 3):
        if sum(readings[i+1:i+4]) > sum(readings[i:i+3]):
            num_increases += 1
    return num_increases

print("part 1: %i increases" % part1(readings))
print("part 2: %i increases" % part2(readings))
