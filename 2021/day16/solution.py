#!/usr/bin/env python3

import sys

from dataclasses import dataclass
from typing import Any

class Bitarray:
    def __init__(self, bits):
        self.bits = bits

    def int(self):
        return int(str(self), 2)

    def hex(self):
        return hex(self.int())

    def split(self, left_bits):
        return Bitarray(self.bits[:left_bits]), Bitarray(self.bits[left_bits:])

    @staticmethod
    def fromhex(h):
        binarystr = bin(int(h, 16))[2:].zfill(4*len(h))
        return Bitarray([c for c in binarystr])

    def __str__(self):
        return "".join(self.bits)

    def __repr__(self):
        return self.hex()

    def __len__(self):
        return len(self.bits)

@dataclass
class Packet:
    version: int
    type_id: int
    payload: Any

    def eval(self):
        if self.type_id == 0:
            return sum([p.eval() for p in self.payload])
        elif self.type_id == 1:
            total = 1
            for p in self.payload:
                total *= p.eval()
            return total
        elif self.type_id == 2:
            return min([p.eval() for p in self.payload])
        elif self.type_id == 3:
            return max([p.eval() for p in self.payload])
        elif self.type_id == 4:
            return self.payload
        elif self.type_id == 5:
            return 1 if self.payload[0].eval() > self.payload[1].eval() else 0
        elif self.type_id == 6:
            return 1 if self.payload[0].eval() < self.payload[1].eval() else 0
        elif self.type_id == 7:
            return 1 if self.payload[0].eval() == self.payload[1].eval() else 0

    def sum_versions(self):
        if self.type_id == 4:
            return self.version
        return self.version + sum([p.sum_versions() for p in self.payload])

    @staticmethod
    def frombits(bits: Bitarray):
        if len(bits) == 0:
            return None, None
        version, bits = bits.split(3)
        version = version.int()
        type_id, bits = bits.split(3)
        type_id = type_id.int()
        payload = None
        if type_id == 4:
            more = True
            payload = 0
            while more:
                intdata, bits = bits.split(5)
                hasmore, intdata = intdata.split(1)
                payload = payload << 4
                payload |= intdata.int()
                if not hasmore.int():
                    more = False
        else:
            length_type_id, bits = bits.split(1)
            length_type_id = length_type_id.int()
            if length_type_id:
                num_packets, bits = bits.split(11)
                num_packets = num_packets.int()
                payload = []
                for i in range(num_packets):
                    new_payload, bits = Packet.frombits(bits)
                    payload.append(new_payload)
            else:
                bit_length, bits = bits.split(15)
                bit_length = bit_length.int()
                subpackets, bits = bits.split(bit_length)
                payload = []
                new_payload, subpackets = Packet.frombits(subpackets)
                while new_payload:
                    payload.append(new_payload)
                    new_payload, subpackets = Packet.frombits(subpackets)
        return Packet(version=version, type_id=type_id, payload=payload), bits

def part1(line):
    b = Bitarray.fromhex(line)
    packet, _ = Packet.frombits(b)
    return packet.sum_versions()

def part2(line):
    b = Bitarray.fromhex(line)
    packet, _ = Packet.frombits(b)
    return packet.eval()

file_name = sys.argv[1]
with open(file_name) as f:
    line = f.readline().strip()

print("part1: sum versions = %d" % part1(line))
print("part2: packet eval = %d" % part2(line))
