#!/usr/bin/env python3

import sys

FIRST = '([{<'
LAST = ')]}>'
CORRUPT_VALUE = [3, 57, 1197, 25137]
COMPLETE_VALUE = [1, 2, 3, 4]

def check(line: str) -> str:
    stack = []
    for c in line:
        if c in FIRST:
            stack.append(c)
        else:
            index = LAST.find(c)
            prev = stack.pop()
            if prev != FIRST[index]:
                return c
    return ""

def complete(line: str) -> str:
    stack = []
    for c in line:
        if c in FIRST:
            stack.append(c)
        else:
            index = LAST.find(c)
            prev = stack.pop()
            if prev != FIRST[index]:
                return ""
    stack.reverse()
    output = ""
    for c in stack:
        output += LAST[FIRST.find(c)]
    return output

def part1(lines):
    score = 0
    for line in lines:
        corrupt = check(line)
        if corrupt == "":
            continue
        score += CORRUPT_VALUE[LAST.find(corrupt)]
    return score

def part2(lines):
    scores = []
    for line in lines:
        if check(line) != "":
            continue
        end = complete(line)
        score = 0
        for c in end:
            score *= 5
            score += COMPLETE_VALUE[LAST.find(c)]
        scores.append(score)
    scores.sort()
    return scores[len(scores)//2]

file_name = sys.argv[1]
with open(file_name) as f:
    lines = f.readlines()
    lines = [l.strip() for l in lines]

print("part 1: score = %i" % part1(lines))
print("part 2: score = %s" % part2(lines))
