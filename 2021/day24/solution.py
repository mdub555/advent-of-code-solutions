#!/usr/bin/env python3

import copy
import cProfile
import math
import operator
import sys

REGISTERS = "wxyz"
OPERATIONS = {
    "add": operator.add,
    "mul": operator.mul,
    "div": operator.ifloordiv,
    "mod": operator.mod,
    "eql": operator.eq,
}

class Op:
    def __init__(self, op, dest, src):
        self.op = op
        self.dest = dest
        self.src = src

    @staticmethod
    def fromline(line):
        line = line.split()
        op = line[0]
        dest = line[1]
        try:
            src = int(line[2])
        except ValueError:
            src = line[2]
        except IndexError:
            src = 0
        return Op(op, dest, src)

    def __str__(self):
        return "{} {} {}".format(self.op, self.dest, self.src)

#Alu = collections.namedtuple("Alu", ["w", "x", "y", "z"], defaults=[Register(), Register(), Register(), Register()])
class AluState:
    def __init__(self):
        self.regs = [0, 0, 0, 0]
        self.loval = 0
        self.hival = 0

    def __eq__(self, other):
        return isinstance(other, AluState) and self.regs == other.regs

    def __hash__(self):
        return hash(tuple(self.regs))

    def __str__(self):
        return " ".join([str(r) for r in self.regs])

    def __repr__(self):
        return str(self)

def process(alu, op):
    dest = REGISTERS.index(op.dest)
    src = op.src if type(op.src) is int else alu.regs[REGISTERS.index(op.src)]
    if op.op == "inp":
        alu.regs[dest] = src
    else:
        alu.regs[dest] = OPERATIONS[op.op](alu.regs[dest], src)

def toarr(i):
    output = []
    while i:
        output.append(i%10)
        i //= 10
    output.reverse()
    return output

def minimize(alus):
    indices = {}
    counter = 0
    for i in range(len(alus)):
        index = indices.get(alus[i])
        if index is None:
            indices[alus[i]] = counter
            alus[counter] = alus[i]
            counter += 1
        else:
            alus[index].hival = max(alus[index].hival, alus[i].hival)
            alus[index].loval = min(alus[index].loval, alus[i].loval)
    del alus[counter:]

def calculate(ops):
    alus = [AluState()]
    max_states = 1
    for op in ops:
        if op.op == "inp":
            minimize(alus)
            for i in range(len(alus)):
                for j in range(2, 10):
                    new_alu = copy.deepcopy(alus[i])
                    new_alu.regs[0] = j
                    new_alu.loval = new_alu.loval * 10 + j
                    new_alu.hival = new_alu.hival * 10 + j
                    alus.append(new_alu)

                alus[i].regs[0] = 1
                alus[i].loval = new_alu.loval * 10 + 1
                alus[i].hival = new_alu.hival * 10 + 1
            max_states *= 9
            print("processing {} alu states instead of {}".format(len(alus), max_states))
        else:
            for alu in alus:
                process(alu, op)
    high = 0
    low = math.inf
    for alu in alus:
        if alu.regs[3] == 0:
            high = max(high, alu.value)
            low = min(low, alu.value)
    return high, low

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]
ops = [Op.fromline(line) for line in lines]

with cProfile.Profile() as pr:
    high, low = calculate(ops)
pr.print_stats()

print("Highest possible: {}".format(high))
print("Lowest possible: {}".format(low))
