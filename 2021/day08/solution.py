#!/usr/bin/env python3

import sys

# Default mapping
#    aaaa
#   b    c
#   b    c
#    dddd
#   e    f
#   e    f
#    gggg

class Symbol:
    def __init__(self, segments: str):
        self.segments = segments

    def decode(self, seg_map) -> int:
        temp = [seg_map[segment] for segment in self.segments]
        temp.sort()
        correct_segments = "".join(temp)
        symbols = {
            'abcefg': 0,
            'cf': 1,
            'acdeg': 2,
            'acdfg': 3,
            'bcdf': 4,
            'abdfg': 5,
            'abdefg': 6,
            'acf': 7,
            'abcdefg': 8,
            'abcdfg': 9
        }
        return symbols[correct_segments]

class Entry:
    def __init__(self, inputs, outputs):
        self.inputs = inputs
        self.outputs = outputs
        self.map = dict()

    @staticmethod
    def parse(line: str):
        parts = line.split(" | ")
        inputs = [Symbol(x) for x in parts[0].split()]
        outputs = [Symbol(x) for x in parts[1].split()]
        return Entry(inputs, outputs)

    def evaluate(self):
        if len(self.map) == 7:
            return
        # These methods aren't independent, order matters!
        self._determine_top_segment()
        self._determine_right_segments()
        self._determine_left_segments()
        self._determine_middle_segment()
        self._determine_bottom_segment()

    def count_outputs(self, num_segments):
        return len(self._get(num_segments, self.outputs))

    def decode_output(self):
        self.evaluate()
        output = ""
        for symbol in self.outputs:
            output += str(symbol.decode(self.map))
        return int(output)

    def _reverse_map(self):
        return {v: k for k, v in self.map.items()}

    def _get(self, num_segments: int, symbols):
        return list(filter(lambda x: len(x.segments) == num_segments, symbols))

    def _determine_top_segment(self):
        one = self._get(2, self.inputs)[0]
        seven = self._get(3, self.inputs)[0]
        for segment in seven.segments:
            if segment not in one.segments:
                self.map[segment] = 'a'
                return

    def _determine_right_segments(self):
        one = self._get(2, self.inputs)[0]
        len6 = self._get(6, self.inputs)
        for symbol in len6:
            for segment in one.segments:
                if segment not in symbol.segments:
                    top_right = segment
                    self.map[segment] = 'c'
        for segment in one.segments:
            if segment != top_right:
                self.map[segment] = 'f'

    def _determine_left_segments(self):
        len5 = self._get(5, self.inputs)
        top_right = self._reverse_map()['c']
        bot_right = self._reverse_map()['f']
        for symbol in len5:
            if top_right not in symbol.segments:
                five = symbol
            if bot_right not in symbol.segments:
                two = symbol

        for segment in five.segments:
            if segment not in two.segments and segment != bot_right:
                self.map[segment] = 'b'
                break

        for segment in two.segments:
            if segment not in five.segments and segment != top_right:
                self.map[segment] = 'e'
                break

    def _determine_middle_segment(self):
        four = self._get(4, self.inputs)[0]
        for segment in four.segments:
            if segment not in self.map:
                self.map[segment] = 'd'
                return

    def _determine_bottom_segment(self):
        eight = self._get(7, self.inputs)[0]
        for segment in eight.segments:
            if segment not in self.map:
                self.map[segment] = 'g'
                return

    def __str__(self):
        return str(self.map)

    def __repr__(self):
        return str(self)

def part1(entries):
    unique_lengths = [2, 3, 4, 7] # for 1, 7, 4, and 8, respectively
    total = 0
    for entry in entries:
        for length in unique_lengths:
            total += entry.count_outputs(length)
    return total

def part2(entries):
    # compare 1 and 7: extra one is top segment.
    # compare 1 and len(6): if a len(6) doesn't contain both 1 segments, that symbol is a 6 and the missing 1 segment is top right.
    # this means the other 1 segment is bottom right.
    # get 5 (missing top right) and 2 (missing bottom right). Each have 3 unknown segments, sharing 2. Unique segment from 5 is top left, unique segment from 2 is bottom left.
    # Unknown segment from 4 is middle.
    # Remaining segment is bottom. All segments known.
    total = 0
    for entry in entries:
        total += entry.decode_output()
    return total

entries = []
file_name = sys.argv[1]
with open(file_name) as f:
    for line in f:
        line = line.strip()
        entries.append(Entry.parse(line))

print("part 1: %i unique outputs" % part1(entries))
print("part 2: %i output total" % part2(entries))
