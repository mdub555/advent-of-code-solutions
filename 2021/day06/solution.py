#!/usr/bin/env python3

import sys

from dataclasses import dataclass, field
from heapq import heappop, heappush, heapify

@dataclass(order=True)
class Group:
    spawn_time: int
    size: int = field(compare=False)

class Population:
    def __init__(self, groups=None):
        self.groups = [] if groups is None else groups
        heapify(self.groups)

    def sum(self):
        total = 0
        for group in self.groups:
            total += group.size
        return total

    def iterate(self):
        spawner = heappop(self.groups)
        self._add_group(Group(spawn_time=spawner.spawn_time+7, size=spawner.size))
        self._add_group(Group(spawn_time=spawner.spawn_time+9, size=spawner.size))

    def curr_time(self):
        return self.groups[0].spawn_time

    def _add_group(self, group: Group):
        for curr_group in groups:
            if curr_group.spawn_time == group.spawn_time:
                curr_group.size += group.size
                return
        heappush(self.groups, group)

    def __str__(self):
        return str(self.groups)

def part1(population):
    while p.curr_time() < 80:
        p.iterate()
    return p.sum()

def part2(population):
    while p.curr_time() < 256:
        p.iterate()
    return p.sum()

file_name = sys.argv[1]
with open(file_name) as f:
    for line in f:
        start = [int(x) for x in line.strip().split(',')]

temp = dict()
for fish in start:
    if fish not in temp:
        temp[fish] = 1
    else:
        temp[fish] += 1

groups = []
for key in temp:
    groups.append(Group(spawn_time=key, size=temp[key]))

p = Population(groups)

print("part 1: population = %i after 80 days" % part1(p))
print("part 1: population = %i after 256 days" % part2(p))
