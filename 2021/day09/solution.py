#!/usr/bin/env python3

import sys

from queue import Queue

def get_mins(grid):
    mins = []
    for row in range(len(grid)):
        for col in range(len(grid[0])):
            cell = grid[row][col]
            if row > 0 and cell >= grid[row-1][col]:
                continue
            if row < len(grid)-1 and cell >= grid[row+1][col]:
                continue
            if col > 0 and cell >= grid[row][col-1]:
                continue
            if col < len(grid[0])-1 and cell >= grid[row][col+1]:
                continue
            mins.append((row, col))
    return mins

def risk(grid):
    mins = get_mins(grid)
    risk = 0
    for m in mins:
        risk += int(grid[m[0]][m[1]]) + 1
    return risk

def flood_fill(grid, start):
    visited = [[False for _ in range(len(grid[0]))] for _ in range(len(grid))]
    queue = Queue()
    queue.put(start)
    size = 0
    while not queue.empty():
        cell = queue.get()
        row, col = cell[0], cell[1]
        if visited[row][col]:
            continue
        size += 1
        visited[row][col] = True
        if row > 0 and not visited[row-1][col] and int(grid[row-1][col]) < 9:
            queue.put((row-1, col))
        if row < len(grid)-1 and not visited[row+1][col] and int(grid[row+1][col]) < 9:
            queue.put((row+1, col))
        if col > 0 and not visited[row][col-1] and int(grid[row][col-1]) < 9:
            queue.put((row, col-1))
        if col < len(grid[0])-1 and not visited[row][col+1] and int(grid[row][col+1]) < 9:
            queue.put((row, col+1))
    return size

def part2(grid):
    biggest = [0, 0, 0]
    mins = get_mins(grid)
    for m in mins:
        basin_size = flood_fill(grid, m)
        if basin_size > biggest[0]:
            biggest[0] = basin_size
            biggest.sort()
    return biggest[0] * biggest[1] * biggest[2]

grid = []
file_name = sys.argv[1]
with open(file_name) as f:
    for line in f:
        line = line.strip()
        grid.append(line)

print("part 1: risk = %i" % risk(grid))
print("part 2: top 3 basins = %i" % part2(grid))
