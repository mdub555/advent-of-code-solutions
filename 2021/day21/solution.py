#!/usr/bin/env python3

import sys

from dataclasses import dataclass, field

class Die:
    def __init__(self):
        self.face = 0
        self.max = 100
        self.rolls = 0

    def roll(self):
        self.rolls += 1
        self.face += 1
        if self.face > self.max:
            self.face = 1
        return self.face

@dataclass
class Player:
    position: int
    score: int = 0

    def move(self, die: Die):
        spaces = sum([die.roll() for _ in range(3)])
        self.position = (self.position + spaces) % 10
        self.score += self.position + 1

@dataclass(eq=True, frozen=True)
class Game:
    positions: tuple[int]
    scores: tuple[int]
    current_player: int = 0

    def move(self, spaces):
        if self.current_player == 0:
            positions = ((self.positions[0]+spaces)%10, self.positions[1])
            scores = (self.scores[0]+positions[0]+1, self.scores[1])
        else:
            positions = (self.positions[0], (self.positions[1]+spaces)%10)
            scores = (self.scores[0], self.scores[1]+positions[1]+1)
        current_player = 1 - self.current_player
        return Game(positions, scores, current_player)

    def maxscore(self):
        return max(self.scores)

    def winner(self):
        if self.scores[0] > self.scores[1]:
            return 0
        else:
            return 1

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "\n---%d---\n0: %d %d\n1: %d %d\n" % (self.current_player, self.positions[0], self.scores[0], self.positions[1], self.scores[1])

def part1(lines):
    d = Die()
    players = [Player(int(line.split()[4])-1) for line in lines]
    win = None
    while win is None:
        for i in range(len(players)):
            players[i].move(d)
            if players[i].score >= 1000:
                win = i
                break
    return d.rolls * players[1-win].score

def part2(lines):
    positions = tuple(int(line.split()[4])-1 for line in lines)
    d = dict()
    wins = [0, 0]
    d[Game(positions, (0, 0))] = 1
    while len(d) > 0:
        oldgame, value = list(d.items())[0]
        del d[oldgame]
        for roll1 in range(1, 4):
            for roll2 in range(1, 4):
                for roll3 in range(1, 4):
                    totalroll = roll1 + roll2 + roll3
                    newgame = oldgame.move(totalroll)
                    if newgame.maxscore() >= 21:
                        wins[newgame.winner()] += value
                    elif newgame in d:
                        d[newgame] += value
                    else:
                        d[newgame] = value
    return max(wins)

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]

print("part1: losing value = %d" % part1(lines))
print("part2: most wins = %d" % part2(lines))
