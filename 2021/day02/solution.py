#!/usr/bin/env python3

import sys

from dataclasses import dataclass

@dataclass
class Command:
    direction: str
    distance: int

file_name = sys.argv[1]
commands = []
with open(file_name) as f:
    for line in f:
        line = line.strip().split()
        commands.append(Command(direction=line[0], distance=int(line[1])))

def part1(commands):
    depth = 0
    horiz = 0
    for command in commands:
        if command.direction == "forward":
            horiz += command.distance
        elif command.direction == "up":
            depth -= command.distance
        elif command.direction == "down":
            depth += command.distance
        else:
            print("Unexpected command: %s" % command)
    return depth * horiz

def part2(commands):
    aim = 0
    depth = 0
    horiz = 0
    for command in commands:
        if command.direction == "forward":
            horiz += command.distance
            depth += aim * command.distance
        elif command.direction == "up":
            aim -= command.distance
        elif command.direction == "down":
            aim += command.distance
        else:
            print("Unexpected command: %s" % command)
    return depth * horiz

print("part 1: %i depth * height" % part1(commands))
print("part 2: %i depth * height" % part2(commands))
