#!/usr/bin/env python3

import sys

from copy import deepcopy

class Grid:
    def __init__(self, grid):
        self.grid = grid
        self.buffer = deepcopy(grid)
        self.current = 0

    def __str__(self):
        output = ""
        for row in self.grid:
            output += "".join(row) + "\n"
        return output

    def moveeast(self):
        moved = False
        copy(self.grid, self.buffer)
        for r in range(len(self.buffer)):
            for c in range(len(self.buffer[r])):
                if self.buffer[r][c] != ">":
                    continue
                newcol = c + 1
                if newcol >= len(self.grid[r]):
                    newcol = 0
                if self.buffer[r][newcol] == ".":
                    self.grid[r][newcol] = ">"
                    self.grid[r][c] = "."
                    moved = True
                    c += 1
        return moved

    def movesouth(self):
        moved = False
        copy(self.grid, self.buffer)
        for r in range(len(self.buffer)):
            for c in range(len(self.buffer[r])):
                if self.buffer[r][c] != "v":
                    continue
                newrow = r + 1
                if newrow >= len(self.grid):
                    newrow = 0
                if self.buffer[newrow][c] == ".":
                    self.grid[newrow][c] = "v"
                    self.grid[r][c] = "."
                    moved = True
        return moved

    def step(self):
        moved = self.moveeast()
        return self.movesouth() or moved

def copy(source, dest):
    for r in range(len(source)):
        for c in range(len(source[r])):
            dest[r][c] = source[r][c]

def part1(rows):
    g = Grid(rows)
    steps = 1
    while g.step():
        steps += 1
    return steps

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]
    rows = [[c for c in line] for line in lines]

print("part1: none moved on step %d" % part1(rows))
