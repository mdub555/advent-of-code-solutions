#!/usr/bin/env python3

import sys

class Board:
    def __init__(self, board):
        self.board = board
        self.mask = [[False for _ in range(5)] for _ in range(5)]
        self.last_move = None

    def add(self, move: int):
        self.last_move = move
        location = self._find(move)
        if location is not None:
            self.mask[location[0]][location[1]] = True

    def check(self) -> bool:
        for i in range(5):
            if self._check_row(i) or self._check_col(i):
                return True
        return False

    def score(self) -> int:
        return self.last_move * self._sum_unmarked()

    def _check_row(self, row: int) -> bool:
        for i in range(5):
            if not self.mask[row][i]:
                return False
        return True

    def _check_col(self, col: int) -> bool:
        for i in range(5):
            if not self.mask[i][col]:
                return False
        return True

    def _find(self, move: int) -> tuple:
        for i in range(5):
            for j in range(5):
                if self.board[i][j] == move:
                    return (i, j)
        return None

    def _sum_unmarked(self) -> int:
        total = 0
        for i in range(5):
            for j in range(5):
                if not self.mask[i][j]:
                    total += self.board[i][j]
        return total

    def __str__(self):
        output = ""
        for row in self.board:
            output += str(row) + "\n"
        output += "\n"
        for row in self.mask:
            output += str(row) + "\n"
        return output

    def __repr__(self):
        return str(self)

def first_win(boards, moves) -> Board:
    for move in moves:
        for board in boards:
            board.add(move)
            if board.check():
                return board

def last_win(boards, moves) -> Board:
    for move in moves:
        for board in boards[::-1]:
            board.add(move)
            if board.check():
                if len(boards) == 1:
                    return board
                boards.remove(board)

def part1(boards, moves) -> int:
    win = first_win(boards, moves)
    return win.score()

def part2(boards, moves) -> int:
    lose = last_win(boards, moves)
    return lose.score()

file_name = sys.argv[1]
boards = []
with open(file_name) as f:
    for line in f:
        moves = [int(x) for x in line.strip().split(",")]
        break
    board = []
    for line in f:
        line = line.strip()
        if line == "":
            if len(board) > 0:
                boards.append(Board(board))
                board = []
            continue
        board.append([int(x) for x in line.split()])
    boards.append(Board(board))

print("part 1: board wins with score %i" % part1(boards, moves))
print("part 2: board loses with score %i" % part2(boards, moves))
