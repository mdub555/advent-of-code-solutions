#!/usr/bin/env python3

import copy
import sys

from dataclasses import dataclass, field

@dataclass
class Image:
    grid: list[list[bool]]
    enhancer: list[bool] = field(repr=False)
    outside: bool = field(default=False)

    def copyandincrease(self):
        new = copy.deepcopy(self.grid)
        for row in new:
            row.insert(0, self.outside)
            row.append(self.outside)
        fullrow = [self.outside for _ in range(len(new[0]))]
        new.insert(0, fullrow)
        new.append(fullrow.copy())
        return new

    def getenhancement(self, row, col, original):
        bits = []
        i = 0
        for r in range(row-1, row+2):
            for c in range(col-1, col+2):
                i += 1
                if r < 0 or r > len(original)-1:
                    bits.append(self.outside)
                elif c < 0 or c > len(original[0])-1:
                    bits.append(self.outside)
                else:
                    bits.append(original[r][c])
        bitstr = ""
        for bit in bits:
            bitstr += "1" if bit else "0"
        index = int(bitstr, 2)
        return self.enhancer[index]

    def enhance(self):
        original = self.copyandincrease()
        new = copy.deepcopy(original)
        for row in range(len(new)):
            for col in range(len(new[0])):
                new[row][col] = self.getenhancement(row, col, original)
        if self.outside:
            outside = self.enhancer[511]
        else:
            outside = self.enhancer[0]
        return Image(new, self.enhancer, outside)

    def litpixels(self):
        return sum([sum(row) for row in self.grid])

    def __str__(self):
        output = ""
        for line in self.grid:
            for c in line:
                output += "#" if c else "."
            output += "\n"
        return output[:-1]

def part1(image):
    image = image.enhance()
    image = image.enhance()
    return image.litpixels()

def part2(image):
    for i in range(50):
        image = image.enhance()
    return image.litpixels()

file_name = sys.argv[1]
with open(file_name) as f:
    enhancerline = f.readline().strip()
    f.readline()
    gridlines = [l.strip() for l in f.readlines()]
enhancer = [True if c == "#" else False for c in enhancerline]
grid = [[True if c == "#" else False for c in line] for line in gridlines]

i = Image(grid, enhancer)
print("part1: %d lit pixels" % part1(i))
print("part2: %d lit pixels" % part2(i))
