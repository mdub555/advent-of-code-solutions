#!/usr/bin/env python3

import sys

from collections.abc import Callable
from dataclasses import dataclass

@dataclass
class Line:
    start: tuple[int, int]
    end: tuple[int, int]

class Grid:
    def __init__(self):
        self.grid = [[0]]

    def add(self, line: Line):
        max_row = max(line.start[1], line.end[1])
        max_col = max(line.start[0], line.end[0])
        self._allocate(max_row, max_col)
        if line.start[0] == line.end[0]:
            if line.start[1] > line.end[1]:
                line.start, line.end = line.end, line.start
            for i in range(line.start[1], line.end[1]+1):
                self.grid[i][line.start[0]] += 1
        elif line.start[1] == line.end[1]:
            if line.start[0] > line.end[0]:
                line.start, line.end = line.end, line.start
            for i in range(line.start[0], line.end[0]+1):
                self.grid[line.start[1]][i] += 1
        else:
            if line.start[0] > line.end[0]:
                line.start, line.end = line.end, line.start
            if line.start[1] > line.end[1]:
                # diagonal upwards /
                for i in range(line.end[0] - line.start[0] + 1):
                    self.grid[line.start[1]-i][line.start[0]+i] += 1
            else:
                # diagonal upwards \
                for i in range(line.end[0] - line.start[0] + 1):
                    self.grid[line.start[1]+i][line.start[0]+i] += 1

    def count(self, filterfn: Callable[[int], bool]):
        total = 0
        for row in self.grid:
            for cell in row:
                if filterfn(cell):
                    total += 1
        return total

    def _allocate(self, max_row, max_col):
        if max_col > len(self.grid[0]) - 1:
            width = len(self.grid[0])
            for i in range(len(self.grid)):
                self.grid[i] += [0 for _ in range(max_col - width + 1)]
        if max_row > len(self.grid) - 1:
            self.grid += [[0 for _ in range(len(self.grid[0]))] for _ in range(max_row - len(self.grid) + 1)]

    def __str__(self):
        output = ""
        for row in self.grid:
            for cell in row:
                if cell == 0:
                    output += "."
                else:
                    output += str(cell)
            output += "\n"
        return output

def part1(lines):
    g = Grid()
    for line in lines:
        g.add(line)
    return g.count(lambda x: x > 1)

def part2(lines):
    # this is the same as part 1, the change was adding to the
    # ifelse statement in Grid.add()
    g = Grid()
    for line in lines:
        g.add(line)
    return g.count(lambda x: x > 1)

lines = []
file_name = sys.argv[1]
with open(file_name) as f:
    for line in f:
        line = line.strip()
        points = line.split(' -> ')
        p0 = points[0].split(',')
        p1 = points[1].split(',')
        l = Line(start=(int(p0[0]), int(p0[1])), end=(int(p1[0]), int(p1[1])))
        lines.append(l)

print("part 1: %i cells with value > 2" % part1(lines))
print("part 2: %i cells with value > 2" % part2(lines))
