#!/usr/bin/env python3

import sys
import time

from dataclasses import dataclass, field

@dataclass
class Beacon:
    x: int
    y: int
    z: int

    def sqrdist(self, other) -> float:
        dx, dy, dz = self.difference(other)
        return dx*dx + dy*dy + dz*dz

    def difference(self, other):
        return self.x - other.x, self.y - other.y, self.z - other.z

    def rotatex(self):
        self.x, self.y, self.z = self.x, -self.z, self.y

    def rotatey(self):
        self.x, self.y, self.z = self.z, self.y, -self.x

    def rotatez(self):
        self.x, self.y, self.z = -self.y, self.x, self.z

    def translate(self, x, y, z):
        self.x += x
        self.y += y
        self.z += z

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __str__(self):
        return "{} {} {}".format(self.x, self.y, self.z)

    @staticmethod
    def fromline(line):
        dims = [int(x) for x in line.split(',')]
        x, y, z = dims[0], 0, 0
        if len(dims) > 1:
            y = dims[1]
        if len(dims) > 2:
            z = dims[2]
        return Beacon(x=x, y=y, z=z)

class Scanner:
    def __init__(self):
        self.beacons = []
        self.distances = set()
        self.disttobeacons = {}
        self.pos = Beacon(0, 0, 0)

    def rotatex(self):
        for beacon in self.beacons:
            beacon.rotatex()

    def rotatey(self):
        for beacon in self.beacons:
            beacon.rotatey()

    def rotatez(self):
        for beacon in self.beacons:
            beacon.rotatez()

    def addbeacon(self, beacon):
        for b in self.beacons:
            dist = b.sqrdist(beacon)
            self.distances.add(dist)
            if dist in self.disttobeacons:
                self.disttobeacons[dist].append((b, beacon))
            else:
                self.disttobeacons[dist] = [(b, beacon)]
        self.beacons.append(beacon)

    def overlap(self, other):
        return len(self.distances & other.distances)

    def translation(self, other):
        matchingdists = self.distances & other.distances
        possibledifferences = {}
        for dist in matchingdists:
            for p1 in self.disttobeacons[dist]:
                for p2 in other.disttobeacons[dist]:
                    dx1, dy1, dz1 = p1[0].difference(p1[1])
                    dx2, dy2, dz2 = p2[0].difference(p2[1])
                    if dx1 == dx2 and dy1 == dy2 and dz1 == dz2:
                        # p1[0] aligns with p2[0]
                        dx, dy, dz = p1[0].difference(p2[0])
                        if (dx, dy, dz) in possibledifferences:
                            possibledifferences[(dx, dy, dz)] += 1
                        else:
                            possibledifferences[(dx, dy, dz)] = 1
                    elif dx1 == -dx2 and dy1 == -dy2 and dz1 == -dz2:
                        # p1[0] aligns with p2[1]
                        dx, dy, dz = p1[0].difference(p2[1])
                        if (dx, dy, dz) in possibledifferences:
                            possibledifferences[(dx, dy, dz)] += 1
                        else:
                            possibledifferences[(dx, dy, dz)] = 1
        dx = dy = dz = None
        maxcount = 65 # at least 66 pairs must agree (12 matching beacons)
        for difference, count in possibledifferences.items():
            if count > maxcount:
                maxcount = count
                dx, dy, dz = difference
        return dx, dy, dz

    def matches(self, other):
        dx, dy, dz = self.translation(other)
        return dx is not None

    def rotatetilmatch(self, other):
        for _ in range(2):
            for i in range(4):
                if self.matches(other):
                    return True
                other.rotatex()
            other.rotatey()
            for i in range(4):
                if self.matches(other):
                    return True
                other.rotatez()
            other.rotatey()
        for _ in range(2):
            other.rotatez()
            for i in range(4):
                if self.matches(other):
                    return True
                other.rotatey()
            other.rotatez()
        return False

    def merge(self, other):
        if not self.rotatetilmatch(other):
            raise ValueError("Couldn't find match")
        dx, dy, dz = self.translation(other)
        other.pos = Beacon(dx, dy, dz)
        for beacon in other.beacons:
            beacon.translate(dx, dy, dz)
            if beacon not in self.beacons:
                self.addbeacon(beacon)

    def __repr__(self):
        return str(self)

    def __str__(self):
        output = "--- scanner ---\n"
        for beacon in self.beacons:
            output += str(beacon) + "\n"
        return output[:-1]

def parse_scanners(lines):
    scanners = []
    for line in lines:
        if line == '':
            continue
        if line.startswith('---'):
            scanner = Scanner()
            scanners.append(scanner)
            continue
        scanner.addbeacon(Beacon.fromline(line))
    return scanners

def part1(lines):
    scanners = parse_scanners(lines)
    main = scanners[0]
    del scanners[0]
    while len(scanners) > 0:
        for i in range(len(scanners)-1, -1, -1):
            if main.overlap(scanners[i]) >= 66:
                try:
                    main.merge(scanners[i])
                    del scanners[i]
                except ValueError:
                    pass
    return len(main.beacons)

def part2(lines):
    unprocessed = parse_scanners(lines)
    processed = [unprocessed[0]]
    main = unprocessed[0]
    del unprocessed[0]
    while len(unprocessed) > 0:
        for i in range(len(unprocessed)-1, -1, -1):
            if main.overlap(unprocessed[i]) >= 66:
                try:
                    main.merge(unprocessed[i])
                    processed.append(unprocessed[i])
                    del unprocessed[i]
                except ValueError:
                    pass
    maxdist = 0
    for i in range(len(processed)-1):
        for j in range(i+1, len(processed)):
            dx, dy, dz = processed[i].pos.difference(processed[j].pos)
            dist = abs(dx) + abs(dy) + abs(dz)
            maxdist = max(maxdist, dist)
    return maxdist

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]

print("part1: there are {} total beacons".format(part1(lines)))
print("part2: {} max distance between scanners".format(part2(lines)))
