#!/usr/bin/env python3
import sys

def insert_pairs1(polymer: str, rules: dict) -> str:
    output = ""
    for i in range(len(polymer)-1):
        pair = polymer[i:i+2]
        output += polymer[i] + rules[pair]
    output += polymer[len(polymer)-1]
    return output

def insert_pairs2(pair_counts: dict, rules: dict) -> None:
    curr = pair_counts.copy()
    pair_counts.clear()
    for pair, count in curr.items():
        lpair = pair[0] + rules[pair]
        rpair = rules[pair] + pair[1]
        if lpair in pair_counts:
            pair_counts[lpair] += count
        else:
            pair_counts[lpair] = count
        if rpair in pair_counts:
            pair_counts[rpair] += count
        else:
            pair_counts[rpair] = count

def score1(polymer: str) -> int:
    char_count = dict()
    for char in polymer:
        if char in char_count:
            char_count[char] += 1
        else:
            char_count[char] = 1
    most = 0
    least = 99999
    for count in char_count.values():
        if count > most:
            most = count
        if count < least:
            least = count
    return most - least

def score2(pair_counts: dict, template: str) -> int:
    char_count = dict()
    for pair in pair_counts:
        for char in pair:
            if char in char_count:
                char_count[char] += pair_counts[pair]
            else:
                char_count[char] = pair_counts[pair]
    for char in char_count:
        char_count[char] //= 2
    char_count[template[0]] += 1
    char_count[template[len(template)-1]] += 1
    most = 0
    least = 99999999999999999
    for count in char_count.values():
        if count > most:
            most = count
        if count < least:
            least = count
    return most - least

def part1(template: str, rules: dict) -> str:
    for i in range(10):
        template = insert_pairs1(template, rules)
    return score1(template)

def part2(template: str, rules: dict) -> str:
    pair_counts = dict()
    for i in range(len(template)-1):
        pair = template[i:i+2]
        if pair in pair_counts:
            pair_counts[pair] += 1
        else:
            pair_counts[pair] = 1
    for i in range(40):
        insert_pairs2(pair_counts, rules)
    return score2(pair_counts, template)

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]
rules = dict()
template = lines[0]
for line in lines[2:]:
    rule = line.split(' -> ')
    rules[rule[0]] = rule[1]

print("part 1: quantity = %i after 10 steps" % part1(template, rules))
print("part 2: quantity = %i after 40 steps" % part2(template, rules))
