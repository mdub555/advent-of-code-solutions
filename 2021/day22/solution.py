#!/usr/bin/env python3

import re
import sys

from dataclasses import dataclass

@dataclass
class Cuboid:
    xs: tuple[int]
    ys: tuple[int]
    zs: tuple[int]
    on: bool = True

    def volume(self):
        return ((self.xs[1]-self.xs[0]+1)
              * (self.ys[1]-self.ys[0]+1)
              * (self.zs[1]-self.zs[0]+1))

    def intersects(self, other):
        if self.xs[0] > other.xs[1] or self.xs[1] < other.xs[0]:
            return False
        if self.ys[0] > other.ys[1] or self.ys[1] < other.ys[0]:
            return False
        if self.zs[0] > other.zs[1] or self.zs[1] < other.zs[0]:
            return False
        return True

    def contains(self, other):
        if other.xs[0] < self.xs[0] or other.xs[1] > self.xs[1]:
            return False
        if other.ys[0] < self.ys[0] or other.ys[1] > self.ys[1]:
            return False
        if other.zs[0] < self.zs[0] or other.zs[1] > self.zs[1]:
            return False
        return True

# Subtracts the overlapping amount from c1 (returning multiple
# cuboids for c1).
def subtract(c1, c2):
    if not c1.intersects(c2):
        return [c1]
    if c2.contains(c1):
        return []
    splits = []
    if c1.xs[0] < c2.xs[0]:
        splits.append(Cuboid((c1.xs[0], c2.xs[0]-1), c1.ys, c1.zs))
        c1.xs = (c2.xs[0], c1.xs[1])
    if c1.xs[1] > c2.xs[1]:
        splits.append(Cuboid((c2.xs[1]+1, c1.xs[1]), c1.ys, c1.zs))
        c1.xs = (c1.xs[0], c2.xs[1])
    if c1.ys[0] < c2.ys[0]:
        splits.append(Cuboid(c1.xs, (c1.ys[0], c2.ys[0]-1), c1.zs))
        c1.ys = (c2.ys[0], c1.ys[1])
    if c1.ys[1] > c2.ys[1]:
        splits.append(Cuboid(c1.xs, (c2.ys[1]+1, c1.ys[1]), c1.zs))
        c1.ys = (c1.ys[0], c2.ys[1])
    if c1.zs[0] < c2.zs[0]:
        splits.append(Cuboid(c1.xs, c1.ys, (c1.zs[0], c2.zs[0]-1)))
        c1.zs = (c2.zs[0], c1.zs[1])
    if c1.zs[1] > c2.zs[1]:
        splits.append(Cuboid(c1.xs, c1.ys, (c2.zs[1]+1, c1.zs[1])))
        c1.zs = (c1.zs[0], c2.zs[1])
    return splits

def parselines(lines):
    regex = re.compile("^(on|off) x=(-?[0-9]+)..(-?[0-9]+),y=(-?[0-9]+)..(-?[0-9]+),z=(-?[0-9]+)..(-?[0-9]+)$")
    cuboids = []
    for line in lines:
        match = regex.fullmatch(line)
        onoff, x1, x2, y1, y2, z1, z2 = match.groups()
        cuboids.append(Cuboid((int(x1), int(x2)), (int(y1), int(y2)), (int(z1), int(z2)), onoff=="on"))
    return cuboids

def processallcuboids(cuboids):
    final = []
    for cube1 in cuboids:
        for j in range(len(final)-1, -1, -1):
            cube2 = final[j]
            if cube1.intersects(cube2):
                final.remove(cube2)
                sub = subtract(cube2, cube1)
                final += sub
        if cube1.on:
            final.append(cube1)
    return final

def part1(lines):
    cuboids = parselines(lines)
    # add surrounding shell to only look at the cubes inside (50, 50).
    cuboids += [
        Cuboid((-51, -51), (-50, 50), (-50, 50)),
        Cuboid((51, 51), (-50, 50), (-50, 50)),
        Cuboid((-50, 50), (-51, -51), (-50, 50)),
        Cuboid((-50, 50), (51, 51), (-50, 50)),
        Cuboid((-50, 50), (-50, 50), (-51, -51)),
        Cuboid((-50, 50), (-50, 50), (51, 51))
    ]
    processed = processallcuboids(cuboids)
    core = Cuboid((-50, 50), (-50, 50), (-50, 50))
    corecubes = [c for c in processed if c.intersects(core)]
    return sum([c.volume() for c in corecubes])

def part2(line):
    cuboids = parselines(lines)
    processed = processallcuboids(cuboids)
    return sum([c.volume() for c in processed])

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]

print("part1: core has %d on" % part1(lines))
print("part2: there are %d total on" % part2(lines))
