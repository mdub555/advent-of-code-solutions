#!/usr/bin/env python3

import re
import sys

from dataclasses import dataclass, field

@dataclass(frozen=True)
class Rectangle:
    left: int
    right: int
    top: int
    bottom: int

    def contains(self, x: int, y: int):
        return (x >= self.left and x <= self.right
                and y >= self.bottom and y <= self.top)

@dataclass
class Probe:
    x_vel: int = field(repr=False)
    y_vel: int = field(repr=False)
    x: int = field(default=0, init=False)
    y: int = field(default=0, init=False)
    max_y: int = field(default=0, init=False)

    def step(self):
        self.x += self.x_vel
        self.y += self.y_vel
        if self.x_vel:
            self.x_vel -= 1
        self.y_vel -= 1
        if self.y > self.max_y:
            self.max_y = self.y

def parse_line(line) -> Rectangle:
    regex = re.compile("^target area: x=(.*)\.\.(.*), y=(.*)\.\.(.*)$")
    match = regex.fullmatch(line)
    if match is None:
        raise ValueError("Couldn't parse line")
    left = int(match.group(1))
    right = int(match.group(2))
    bottom = int(match.group(3))
    top = int(match.group(4))
    return Rectangle(left=left, right=right, top=top, bottom=bottom)

def launch_probe(probe: Probe, area: Rectangle):
    while probe.x <= area.right and probe.y >= area.bottom:
        if area.contains(probe.x, probe.y):
            return True
        probe.step()
    return False

def get_min_x_vel(area: Rectangle):
    x_vel = 1
    total_range = 1
    while total_range < area.left:
        x_vel += 1
        total_range += x_vel
    return x_vel

def get_max_x_vel(area: Rectangle):
    return area.right + 1

def get_min_y_vel(area: Rectangle):
    return area.bottom - 1

def get_max_y_vel(area: Rectangle):
    return abs(area.bottom) - 1

def part1(line):
    area = parse_line(line)
    probe = Probe(x_vel = get_min_x_vel(area), y_vel = get_max_y_vel(area))
    if not launch_probe(probe, area):
        return -1
    return probe.max_y

def part2(line):
    area = parse_line(line)
    viable_probes = 0
    for x in range(get_min_x_vel(area), get_max_x_vel(area)+1):
        for y in range(get_min_y_vel(area), get_max_y_vel(area)+1):
            probe = Probe(x_vel = x, y_vel = y)
            if launch_probe(probe, area):
                viable_probes += 1
    return viable_probes

file_name = sys.argv[1]
with open(file_name) as f:
    line = f.readlines()[0].strip()

print("part 1: max height = %d" % part1(line))
print("part 2: num viable probes = %d" % part2(line))
