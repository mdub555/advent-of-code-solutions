#!/usr/bin/env python3

import itertools
import math
import sys

from copy import deepcopy
from dataclasses import dataclass, field
from heapq import heappop, heappush, heapify

class PriorityQueue:
    REMOVED = '<removed-task>'

    def __init__(self):
        self.pq = []
        self.entry_finder = {}
        self.counter = itertools.count()

    def push(self, cell, priority):
        if cell in self.entry_finder:
            self._remove(cell)
        count = next(self.counter)
        entry = [priority, count, cell]
        self.entry_finder[cell] = entry
        heappush(self.pq, entry)

    def pop(self):
        while self.pq:
            priority, count, cell = heappop(self.pq)
            if cell is not self.REMOVED:
                del self.entry_finder[cell]
                return cell
        raise ValueError("None left")

    def _remove(self, cell):
        entry = self.entry_finder.pop(cell)
        entry[-1] = self.REMOVED

    def __len__(self):
        return len(self.pq)


@dataclass(order=True)
class Cell:
    value: int = field(compare=False)
    row: int = field(compare=False)
    col: int = field(compare=False)
    cost: int = field(default=math.inf)
    visited: bool = field(default=False, compare=False)

class Grid:
    def __init__(self, values):
        self.values = values
        self.costs = [[math.inf for _ in values[0]] for _ in values]
        self.visited = [[False for _ in values[0]] for _ in values]

    def dijkstra(self):
        self.costs[0][0] = 0

        queue = PriorityQueue()
        for row in range(len(self.values)):
            for col in range(len(self.values[row])):
                queue.push((row, col), self.costs[row][col])
        while len(queue) > 0:
            try:
                row, col = queue.pop()
            except ValueError:
                break
            self.visited[row][col] = True
            if row > 0 and not self.visited[row-1][col]:
                if self.costs[row-1][col] > self.values[row-1][col] + self.costs[row][col]:
                    self.costs[row-1][col] = self.values[row-1][col] + self.costs[row][col]
                    queue.push((row-1, col), self.costs[row-1][col])

            if col > 0 and not self.visited[row][col-1]:
                if self.costs[row][col-1] > self.values[row][col-1] + self.costs[row][col]:
                    self.costs[row][col-1] = self.values[row][col-1] + self.costs[row][col]
                    queue.push((row, col-1), self.costs[row][col-1])

            if row < len(self.values)-1 and not self.visited[row+1][col]:
                if self.costs[row+1][col] > self.values[row+1][col] + self.costs[row][col]:
                    self.costs[row+1][col] = self.values[row+1][col] + self.costs[row][col]
                    queue.push((row+1, col), self.costs[row+1][col])

            if col < len(self.values[row])-1 and not self.visited[row][col+1]:
                if self.costs[row][col+1] > self.values[row][col+1] + self.costs[row][col]:
                    self.costs[row][col+1] = self.values[row][col+1] + self.costs[row][col]
                    queue.push((row, col+1), self.costs[row][col+1])

    def min_cost(self, row, col):
        self.dijkstra()
        return self.costs[row][col]

    def find_path(self):
        pass

def create_grid(lines):
    grid = []
    for line in lines:
        grid.append([int(cell) for cell in line])
    return Grid(grid)

def inc_grid(grid):
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            grid[row][col] += 1
            if grid[row][col] >= 10:
                grid[row][col] = 1

def create_grid2(lines):
    grid = []
    for line in lines:
        grid.append([int(cell) for cell in line])
    og_grid = deepcopy(grid)
    for _ in range(4):
        inc_grid(og_grid)
        for row in range(len(grid)):
            grid[row] += og_grid[row]
    og_grid = deepcopy(grid)
    for _ in range(4):
        inc_grid(og_grid)
        grid += deepcopy(og_grid)
    return Grid(grid)

def to_string(grid_array):
    output = ""
    for row in grid_array:
        for cell in row:
            output += str(cell) + " "
        output += "\n"
    return output[:-1]

def part1(lines):
    grid = create_grid(lines)
    grid.dijkstra()
    return grid.costs[-1][-1]

def part2(lines):
    grid = create_grid2(lines)
    grid.dijkstra()
    return grid.costs[-1][-1]

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]

print("part 1: total risk = %i" % part1(lines))
print("part 2: total risk = %i" % part2(lines))
