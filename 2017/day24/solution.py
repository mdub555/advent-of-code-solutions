#!/usr/bin/env python3

import sys

file_name = sys.argv[1]
with open(file_name) as f:
    lines = [l.strip() for l in f.readlines()]
    # for line in f:
    #     print(line)
