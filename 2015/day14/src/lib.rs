use regex::Regex;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Reindeer {
    speed: u32,
    flytime: u32,
    resttime: u32,
    distance: u32,
    flying: bool,
    next_change: u32,
    points: u32,
}

impl Reindeer {
    fn calc_dist(&self, time: u32) -> u32 {
        let cycles = time / (self.flytime + self.resttime);
        let rem = time % (self.flytime + self.resttime);
        let movingtime = std::cmp::min(self.flytime, rem);
        cycles * self.speed * self.flytime + movingtime * self.speed
    }

    fn from(line: &str) -> Reindeer {
        let re = Regex::new("^[a-zA-Z ]+ ([0-9]+) km/s for ([0-9]+) seconds, but then must rest for ([0-9]+) seconds.$").unwrap();
        let caps = re.captures(line).unwrap();
        let speed: u32 = caps.get(1).unwrap().as_str().parse().unwrap();
        let flytime: u32 = caps.get(2).unwrap().as_str().parse().unwrap();
        let resttime: u32 = caps.get(3).unwrap().as_str().parse().unwrap();
        Reindeer {
            speed,
            flytime,
            resttime,
            distance: 0,
            flying: true,
            next_change: flytime,
            points: 0,
        }
    }

    fn step(&mut self) {
        if self.flying {
            self.distance += self.speed;
        }
        self.next_change -= 1;
        if self.next_change <= 0 {
            if self.flying {
                self.next_change = self.resttime;
            } else {
                self.next_change = self.flytime;
            }
            self.flying = !self.flying;
        }
    }
}

fn part1(contents: &str) {
    let mut max = 0;
    for line in contents.lines() {
        let r = Reindeer::from(line);
        let dist = r.calc_dist(2503);
        if dist > max {
            max = dist;
        }
    }
    println!("Furthest reindeer: {}", max);
}

fn part2(contents: &str) {
    let mut reindeer: Vec<Reindeer> = contents.lines().map(|l| Reindeer::from(l)).collect();
    for i in 0..2503 {
        for deer in &mut reindeer {
            deer.step();
        }
        reindeer.sort_by(|a, b| b.distance.cmp(&a.distance));
        let lead = reindeer[0].distance;
        for deer in &mut reindeer {
            if deer.distance == lead {
                deer.points += 1;
            }
        }
    }
    reindeer.sort_by(|a, b| b.points.cmp(&a.points));
    println!("Most points: {}", reindeer[0].points);
}
