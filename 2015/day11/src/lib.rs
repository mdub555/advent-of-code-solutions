use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn has_straight(password: &str) -> bool {
    let chars: Vec<u8> = password.chars().map(|c| c as u8).collect();
    for i in 0..chars.len() - 2 {
        if chars[i] + 1 == chars[i + 1] && chars[i + 1] + 1 == chars[i + 2] {
            return true;
        }
    }
    false
}

fn contains_invalid_chars(password: &str) -> bool {
    for c in vec!['i', 'o', 'l'] {
        if password.contains(c) {
            return true;
        }
    }
    false
}

fn contains_double_pairs(password: &str) -> bool {
    let chars: Vec<char> = password.chars().collect();
    let mut pairs: usize = 0;
    let mut i: usize = 0;
    loop {
        if i + 1 >= chars.len() {
            break;
        }
        if chars[i] == chars[i + 1] {
            pairs += 1;
            if pairs >= 2 {
                return true;
            }
            i += 1;
        }
        i += 1;
    }
    false
}

fn is_valid(password: &str) -> bool {
    has_straight(&password)
        && !contains_invalid_chars(&password)
        && contains_double_pairs(&password)
}

fn increment(password: &mut String) {
    let mut carry = 1;
    let mut i: usize = password.len() - 1;
    while carry > 0 {
        let c: &str = password.get(i..i + 1).unwrap();
        if c == "z" {
            password.replace_range(i..i + 1, "a");
            i -= 1;
        } else {
            let mut c: u32 = c.chars().next().unwrap() as u32;
            c += 1;
            carry = 0;
            password.replace_range(i..i + 1, &char::from_u32(c).unwrap().to_string());
        }
    }
}

fn part1(contents: &str) {
    let mut password = contents.to_string();
    while !is_valid(&password) {
        increment(&mut password);
    }
    println!("Next password: {}", &password);
}

fn part2(contents: &str) {
    let mut password = contents.to_string();
    while !is_valid(&password) {
        increment(&mut password);
    }
    increment(&mut password);
    while !is_valid(&password) {
        increment(&mut password);
    }
    println!("Next next password: {}", &password);
}
