use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

// This method returns the amount of ways you can put amount
// into containers.
fn fill(amount: u32, containers: &[u32]) -> u32 {
    let mut ways = 0;
    for i in 0..containers.len() {
        if containers[i] > amount {
            continue;
        }
        if containers[i] == amount {
            ways += 1;
            continue;
        }
        ways += fill(amount - containers[i], &containers[i + 1..]);
    }
    ways
}

// This method returns the actual combinations of containers
// you can use to fit amount.
fn fill2(amount: u32, containers: &[u32]) -> Vec<Vec<u32>> {
    let mut ways: Vec<Vec<u32>> = Vec::new();
    for i in 0..containers.len() {
        if containers[i] > amount {
            continue;
        }
        if containers[i] == amount {
            ways.push(vec![containers[i]]);
            continue;
        }
        for way in fill2(amount - containers[i], &containers[i + 1..]) {
            let mut w = way;
            w.push(containers[i]);
            ways.push(w);
        }
    }
    ways
}

fn part1(contents: &str) {
    let mut containers: Vec<u32> = contents.lines().map(|l| l.parse().unwrap()).collect();
    containers.sort_by(|a, b| b.cmp(a));
    println!("{} different combinations.", fill(150, &containers[..]));
}

fn part2(contents: &str) {
    let mut containers: Vec<u32> = contents.lines().map(|l| l.parse().unwrap()).collect();
    containers.sort_by(|a, b| b.cmp(a));
    let combinations = fill2(150, &containers[..]);
    let mut smallest: (usize, usize) = (usize::MAX, 0);
    for combo in &combinations {
        if combo.len() < smallest.0 {
            smallest = (combo.len(), 1);
        } else if combo.len() == smallest.0 {
            smallest.1 += 1;
        }
    }
    println!(
        "{} minimum containers, {} combinations.",
        smallest.0, smallest.1
    );
}
