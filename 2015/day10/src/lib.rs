use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn step(s: &str) -> String {
    let mut result = String::from("");
    let mut c = &s[0..1];
    let mut quant: usize = 0;
    for i in 0..s.len() {
        if &s[i..i + 1] != c {
            result.push_str(&quant.to_string());
            result.push_str(c);
            if i < s.len() {
                c = &s[i..i + 1];
            }
            quant = 0;
        }
        quant += 1;
    }
    result.push_str(&quant.to_string());
    result.push_str(c);
    result
}

fn part1(contents: &str) {
    let mut digits = contents.to_string();
    for _ in 0..40 {
        digits = step(&digits);
    }
    println!("Result length: {}", digits.len());
}

fn part2(contents: &str) {
    let mut digits = contents.to_string();
    for _ in 0..50 {
        digits = step(&digits);
    }
    println!("Result length: {}", digits.len());
}
