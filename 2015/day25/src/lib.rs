use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);

    Ok(())
}

fn f(x: &mut u64) {
    *x = (*x * 252533) % 33554393;
}

fn calc(start: u64, row: u64, col: u64) -> u64 {
    let mut r: u64 = 1;
    let mut c: u64 = 1;
    let mut x: u64 = start;
    while r != row || c != col {
        f(&mut x);
        r -= 1;
        c += 1;
        if r <= 0 {
            r = c;
            c = 1;
        }
    }
    x
}

fn part1(_contents: &str) {
    let start: u64 = 20151125;
    let row: u64 = 2981;
    let col: u64 = 3075;
    println!("{}", calc(start, row, col));
}
