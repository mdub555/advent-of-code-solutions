use itertools::Itertools;
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Matrix {
    cities: HashMap<String, usize>,
    dists: Vec<Vec<Option<usize>>>,
}

impl Matrix {
    fn new() -> Matrix {
        Matrix {
            cities: HashMap::new(),
            dists: Vec::new(),
        }
    }

    fn add(&mut self, line: &str) {
        // Parse line.
        let re = Regex::new("^([a-zA-Z]+) to ([a-zA-Z]+) = ([0-9]+)$").unwrap();
        let caps = re.captures(line).unwrap();
        let city1 = caps.get(1).unwrap().as_str().to_string();
        let city2 = caps.get(2).unwrap().as_str().to_string();
        let dist: usize = caps.get(3).unwrap().as_str().parse().unwrap();

        // Allocate space to hashmap and matrix.
        if !self.cities.contains_key(&city1) {
            let num_cities = self.cities.len();
            self.cities.insert(city1.clone(), num_cities);
            for distance in &mut self.dists {
                distance.push(None);
            }
            self.dists.push(vec![None; num_cities + 1]);
            self.dists[num_cities][num_cities] = Some(0);
        }
        if !self.cities.contains_key(&city2) {
            let num_cities = self.cities.len();
            self.cities.insert(city2.clone(), num_cities);
            for distance in &mut self.dists {
                distance.push(None);
            }
            self.dists.push(vec![None; num_cities + 1]);
            self.dists[num_cities][num_cities] = Some(0);
        }

        // Insert data into hashmap and matrix.
        let i1: usize = *self.cities.get(&city1).unwrap();
        let i2: usize = *self.cities.get(&city2).unwrap();
        self.dists[i1][i2] = Some(dist);
        self.dists[i2][i1] = Some(dist);
    }

    fn sum_dist(&self, order: &Vec<&usize>) -> usize {
        let mut total = 0;
        for i in 0..order.len() - 1 {
            if let Some(d) = self.dists[*order[i]][*order[i + 1]] {
                total += d;
            }
        }
        total
    }

    fn shortest(&self) -> usize {
        let mut locs: Vec<usize> = Vec::new();
        for i in 0..self.cities.len() {
            locs.push(i);
        }
        let mut short = usize::MAX;
        for perm in locs.iter().permutations(locs.len()) {
            let dist = self.sum_dist(&perm);
            if dist < short {
                short = dist;
            }
        }
        short
    }

    fn longest(&self) -> usize {
        let mut locs: Vec<usize> = Vec::new();
        for i in 0..self.cities.len() {
            locs.push(i);
        }
        let mut long: usize = 0;
        for perm in locs.iter().permutations(locs.len()) {
            let dist = self.sum_dist(&perm);
            if dist > long {
                long = dist;
            }
        }
        long
    }
}

fn part1(contents: &str) {
    let mut m = Matrix::new();
    for line in contents.lines() {
        m.add(line);
    }
    println!("Shortest route: {}", m.shortest());
}

fn part2(contents: &str) {
    let mut m = Matrix::new();
    for line in contents.lines() {
        m.add(line);
    }
    println!("Longest route: {}", m.longest());
}
