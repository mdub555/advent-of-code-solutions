use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn part1(contents: &str) {
    let mut current = [0, 0];
    let mut tiles = HashMap::new();
    tiles.insert(current.clone(), 1);
    for c in contents.chars() {
        match c {
            '^' => current[0] -= 1,
            '>' => current[1] += 1,
            'v' => current[0] += 1,
            '<' => current[1] -= 1,
            _ => panic!("Not a valid char"),
        }
        let count = tiles.entry(current.clone()).or_insert(0);
        *count += 1;
    }
    println!("Number of houses: {}", tiles.len());
}

fn part2(contents: &str) {
    let mut current = [[0, 0], [0, 0]];
    let mut tiles = HashMap::new();
    let mut moving = 0;
    tiles.insert(current[0].clone(), 2);
    for c in contents.chars() {
        match c {
            '^' => current[moving][0] -= 1,
            '>' => current[moving][1] += 1,
            'v' => current[moving][0] += 1,
            '<' => current[moving][1] -= 1,
            _ => panic!("Not a valid char"),
        }
        let count = tiles.entry(current[moving].clone()).or_insert(0);
        *count += 1;
        moving = (moving + 1) % 2;
    }
    println!("Number of houses: {}", tiles.len());
}
