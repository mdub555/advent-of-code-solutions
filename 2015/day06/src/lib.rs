use regex::Regex;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
enum Action {
    On,
    Off,
    Toggle,
}

#[derive(Debug)]
struct Rect {
    left: u32,
    top: u32,
    right: u32,
    bottom: u32,
    action: Action,
}

impl Rect {
    fn new(line: &str) -> Rect {
        let re: Regex =
            Regex::new(r"^.*(toggle|on|off) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)$").unwrap();
        let caps = re.captures(line).unwrap();
        let action = match caps.get(1).map_or("", |m| m.as_str()) {
            "toggle" => Action::Toggle,
            "on" => Action::On,
            "off" => Action::Off,
            _ => panic!(),
        };
        let mut left: u32 = 0;
        if let Some(num) = caps.get(2) {
            left = num.as_str().parse::<u32>().unwrap();
        }
        let mut top: u32 = 0;
        if let Some(num) = caps.get(3) {
            top = num.as_str().parse::<u32>().unwrap();
        }
        let mut right: u32 = 0;
        if let Some(num) = caps.get(4) {
            right = num.as_str().parse::<u32>().unwrap();
        }
        let mut bottom: u32 = 0;
        if let Some(num) = caps.get(5) {
            bottom = num.as_str().parse::<u32>().unwrap();
        }
        Rect {
            left,
            top,
            right,
            bottom,
            action,
        }
    }
}

fn process_rect(grid: &mut Vec<Vec<bool>>, rect: &Rect) {
    for row in rect.top..rect.bottom + 1 {
        for col in rect.left..rect.right + 1 {
            let row = row as usize;
            let col = col as usize;
            match rect.action {
                Action::On => grid[row][col] = true,
                Action::Off => grid[row][col] = false,
                Action::Toggle => grid[row][col] = !grid[row][col],
            }
        }
    }
}

fn process_rect2(grid: &mut Vec<Vec<u32>>, rect: &Rect) {
    for row in rect.top..rect.bottom + 1 {
        for col in rect.left..rect.right + 1 {
            let row = row as usize;
            let col = col as usize;
            match rect.action {
                Action::On => grid[row][col] += 1,
                Action::Off => {
                    if grid[row][col] > 0 {
                        grid[row][col] -= 1;
                    }
                }
                Action::Toggle => grid[row][col] += 2,
            }
        }
    }
}

fn part1(contents: &str) {
    let mut grid = vec![vec![false; 1000]; 1000];
    let rects: Vec<Rect> = contents.split("\n").map(|line| Rect::new(line)).collect();
    for rect in rects {
        process_rect(&mut grid, &rect);
    }
    let mut total = 0;
    for row in grid {
        for cell in row {
            if cell {
                total += 1;
            }
        }
    }
    println!("{} lights are on", total);
}

fn part2(contents: &str) {
    let mut grid = vec![vec![0; 1000]; 1000];
    let rects: Vec<Rect> = contents.split("\n").map(|line| Rect::new(line)).collect();
    for rect in rects {
        process_rect2(&mut grid, &rect);
    }
    let mut total = 0;
    for row in grid {
        for cell in row {
            total += cell;
        }
    }
    println!("{} lights are on", total);
}
