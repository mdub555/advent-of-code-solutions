use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn fill(amount: u32, weights: &Vec<u32>) -> Option<Vec<u32>> {
    if weights.contains(&amount) {
        return Some(vec![amount]);
    }
    let mut weights = weights.clone();
    let mut options: Vec<Vec<u32>> = Vec::new();
    for _ in 0..weights.len() {
        let w = weights[0];
        if w > amount {
            continue;
        }
        weights.swap_remove(0);
        if let Some(mut option) = fill(amount - w, &weights) {
            option.push(w);
            options.push(option);
        }
    }
    let mut quantity = usize::MAX;
    let mut best_index = 0;
    for i in 0..options.len() {
        if options[i].len() < quantity
            || (options[i].len() == quantity
                && entanglement(&options[i]) < entanglement(&options[best_index]))
        {
            quantity = options[i].len();
            best_index = i;
        }
    }
    if options.is_empty() {
        return None;
    }
    Some(options.swap_remove(best_index))
}

fn entanglement(weights: &Vec<u32>) -> u64 {
    if weights.is_empty() {
        return 0;
    }
    let mut qe: u64 = 1;
    for weight in weights {
        qe *= *weight as u64;
    }
    qe
}

fn part1(contents: &str) {
    let weights: Vec<u32> = contents.lines().map(|l| l.parse().unwrap()).collect();
    let sum: u32 = weights.iter().sum();
    println!("Sum: {}", sum);
    if let Some(section1) = fill(sum / 3, &weights) {
        println!("{:?}, QE: {:?}", section1, entanglement(&section1));
    }
}

fn part2(contents: &str) {
    let weights: Vec<u32> = contents.lines().map(|l| l.parse().unwrap()).collect();
    let sum: u32 = weights.iter().sum();
    println!("Sum: {}", sum);
    if let Some(section1) = fill(sum / 4, &weights) {
        println!("{:?}, QE: {:?}", section1, entanglement(&section1));
    }
}
