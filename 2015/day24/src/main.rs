use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    let config = day24::Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = day24::run(config) {
        println!("Application error: {}", e);
        process::exit(1);
    }
}
