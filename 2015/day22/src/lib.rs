use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

struct Character {
    hp: i32,
    mana: i32,
    armor: i32,
    damage: i32,
    shielded: i32,
    poisoned: i32,
    recharging: i32,
}

impl Character {
    fn new(hp: i32, mana: i32, damage: i32) -> Character {
        Character {
            hp,
            mana,
            armor: 0,
            damage,
            shielded: 0,
            poisoned: 0,
            recharging: 0,
        }
    }

    fn turn(&mut self, other: &mut Character) {}

    fn damage(&mut self, damage: i32) {
        if self.armor > damage {
            self.hp -= 1;
        } else {
            self.damage -= damage - self.armor;
        }
    }
}

fn part1(contents: &str) {
    let boss = Character::new(71, 0, 10);
    let me = Character::new(50, 500, 0);
}

fn part2(contents: &str) {}
