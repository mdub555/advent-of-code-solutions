use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn part1(contents: &str) {
    let mut hash = md5::compute(contents);
    let mut i: u32 = 0;
    while &format!("{:x}", hash)[0..5] != "00000" {
        i += 1;
        let key = String::from(contents) + &i.to_string();
        hash = md5::compute(key);
    }
    println!("Smallest number = {}, giving hash {:x}", i, hash);
}

fn part2(contents: &str) {
    let mut hash = md5::compute(contents);
    let mut i: u32 = 0;
    while &format!("{:x}", hash)[0..6] != "000000" {
        i += 1;
        let key = String::from(contents) + &i.to_string();
        hash = md5::compute(key);
    }
    println!("Smallest number = {}, giving hash {:x}", i, hash);
}
