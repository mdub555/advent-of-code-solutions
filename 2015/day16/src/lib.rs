use regex::Regex;
use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct AuntSue {
    index: u32,
    compounds: HashMap<String, u32>,
}

impl AuntSue {
    fn new(line: &str) -> AuntSue {
        let re = Regex::new(r"^Sue ([0-9]+): ([a-z0-9,: ]+)$").unwrap();
        let caps = re.captures(line).unwrap();
        let index: u32 = caps.get(1).unwrap().as_str().parse().unwrap();
        let categories = caps.get(2).unwrap().as_str().split(", ");
        let mut compounds = HashMap::new();
        for cat in categories {
            let a: Vec<&str> = cat.split(": ").collect();
            let name = a.get(0).unwrap().to_string();
            let val: u32 = a.get(1).unwrap().to_string().parse().unwrap();
            compounds.insert(name, val);
        }
        AuntSue { index, compounds }
    }

    fn matches(&self, compounds: &HashMap<String, (u32, fn(&u32, &u32) -> bool)>) -> bool {
        for (name, value) in &self.compounds {
            if !compounds.contains_key(name) {
                return false;
            }
            let (v, fun) = compounds.get(name).unwrap();
            if !fun(v, value) {
                return false;
            }
        }
        true
    }
}

fn part1(contents: &str) {
    let eq: fn(&u32, &u32) -> bool = std::cmp::PartialEq::eq;
    let compounds: HashMap<String, (u32, fn(&u32, &u32) -> bool)> = HashMap::from([
        (String::from("children"), (3, eq)),
        (String::from("cats"), (7, eq)),
        (String::from("samoyeds"), (2, eq)),
        (String::from("pomeranians"), (3, eq)),
        (String::from("akitas"), (0, eq)),
        (String::from("vizslas"), (0, eq)),
        (String::from("goldfish"), (5, eq)),
        (String::from("trees"), (3, eq)),
        (String::from("cars"), (2, eq)),
        (String::from("perfumes"), (1, eq)),
    ]);
    for line in contents.lines() {
        let s = AuntSue::new(&line);
        if s.matches(&compounds) {
            println!("Totally matching Sue: {:?}", s);
        }
    }
}

fn part2(contents: &str) {
    let eq: fn(&u32, &u32) -> bool = std::cmp::PartialEq::eq;
    let lt: fn(&u32, &u32) -> bool = std::cmp::PartialOrd::gt;
    let gt: fn(&u32, &u32) -> bool = std::cmp::PartialOrd::lt;
    let compounds: HashMap<String, (u32, fn(&u32, &u32) -> bool)> = HashMap::from([
        (String::from("children"), (3, eq)),
        (String::from("cats"), (7, gt)),
        (String::from("samoyeds"), (2, eq)),
        (String::from("pomeranians"), (3, lt)),
        (String::from("akitas"), (0, eq)),
        (String::from("vizslas"), (0, eq)),
        (String::from("goldfish"), (5, lt)),
        (String::from("trees"), (3, gt)),
        (String::from("cars"), (2, eq)),
        (String::from("perfumes"), (1, eq)),
    ]);
    for line in contents.lines() {
        let s = AuntSue::new(&line);
        if s.matches(&compounds) {
            println!("Loosely matching Sue: {:?}", s);
        }
    }
}
