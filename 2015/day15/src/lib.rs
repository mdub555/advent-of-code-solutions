use std::cmp::max;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Ingredient {
    capacity: i32,
    durability: i32,
    flavor: i32,
    texture: i32,
    calories: u32,
    amount: u32,
}

impl Ingredient {
    fn from(line: &str) -> Ingredient {
        let line = line.replace(",", "");
        let tokens: Vec<&str> = line.split(" ").collect();
        Ingredient {
            capacity: tokens[2].parse().unwrap(),
            durability: tokens[4].parse().unwrap(),
            flavor: tokens[6].parse().unwrap(),
            texture: tokens[8].parse().unwrap(),
            calories: tokens[10].parse().unwrap(),
            amount: 0,
        }
    }
}

#[derive(Debug)]
struct Recipe {
    ingredients: Vec<Ingredient>,
}

impl Recipe {
    fn new() -> Recipe {
        Recipe {
            ingredients: Vec::new(),
        }
    }

    fn score(&self) -> i32 {
        let capacity = max(
            0,
            self.ingredients
                .iter()
                .fold(0, |acc, i| acc + i.capacity * i.amount as i32),
        );
        let durability = max(
            0,
            self.ingredients
                .iter()
                .fold(0, |acc, i| acc + i.durability * i.amount as i32),
        );
        let flavor = max(
            0,
            self.ingredients
                .iter()
                .fold(0, |acc, i| acc + i.flavor * i.amount as i32),
        );
        let texture = max(
            0,
            self.ingredients
                .iter()
                .fold(0, |acc, i| acc + i.texture * i.amount as i32),
        );
        capacity * durability * flavor * texture
    }

    fn calories(&self) -> u32 {
        self.ingredients
            .iter()
            .fold(0, |acc, i| acc + i.calories * i.amount)
    }
}

fn part1(contents: &str) {
    let mut recipe = Recipe::new();
    for line in contents.lines() {
        recipe.ingredients.push(Ingredient::from(&line));
    }
    let mut max = i32::MIN;
    for i in 0..101 {
        recipe.ingredients[0].amount = i;
        for j in 0..101 - i {
            recipe.ingredients[1].amount = j;
            for k in 0..101 - j - i {
                recipe.ingredients[2].amount = k;
                recipe.ingredients[3].amount = 100 - i - j - k;
                let score = recipe.score();
                if score > max {
                    max = score;
                }
            }
        }
    }
    println!("Best score: {}", &max);
}

fn part2(contents: &str) {
    let mut recipe = Recipe::new();
    for line in contents.lines() {
        recipe.ingredients.push(Ingredient::from(&line));
    }
    let mut max = i32::MIN;
    for i in 0..101 {
        recipe.ingredients[0].amount = i;
        for j in 0..101 - i {
            recipe.ingredients[1].amount = j;
            for k in 0..101 - j - i {
                recipe.ingredients[2].amount = k;
                recipe.ingredients[3].amount = 100 - i - j - k;
                if recipe.calories() != 500 {
                    continue;
                }
                let score = recipe.score();
                if score > max {
                    max = score;
                }
            }
        }
    }
    println!("Best score with 500 calories: {}", &max);
}
