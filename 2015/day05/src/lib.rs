use lazy_static::lazy_static;
use regex::Regex;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn contains_three_vowels(text: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r".*[aeiou].*[aeiou].*[aeiou].*").unwrap();
    }
    RE.is_match(text)
}

fn contains_double_letter(text: &str) -> bool {
    let t = text.as_bytes();
    for i in 0..text.len() - 1 {
        if t[i] == t[i + 1] {
            return true;
        }
    }
    false
}

fn doesnt_contain_bad_strings(text: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r".*(?:ab|cd|pq|xy).*").unwrap();
    }
    !RE.is_match(text)
}

fn contains_double_pair(text: &str) -> bool {
    lazy_static! {
        static ref RE: fancy_regex::Regex = fancy_regex::Regex::new(r".*(..).*\1.*").unwrap();
    }
    RE.is_match(text).unwrap()
}

fn contains_split_pair(text: &str) -> bool {
    lazy_static! {
        static ref RE: fancy_regex::Regex = fancy_regex::Regex::new(r".*(.).\1.*").unwrap();
    }
    RE.is_match(text).unwrap()
}

fn part1(contents: &str) {
    let lines: Vec<&str> = contents
        .split("\n")
        .filter(|&line| contains_three_vowels(line))
        .filter(|&line| contains_double_letter(line))
        .filter(|&line| doesnt_contain_bad_strings(line))
        .collect();
    println!("There are {} nice strings.", lines.len());
}

fn part2(contents: &str) {
    let lines: Vec<&str> = contents
        .split("\n")
        .filter(|&line| contains_double_pair(line))
        .filter(|&line| contains_split_pair(line))
        .collect();
    println!("There are {} nice strings.", lines.len());
}
