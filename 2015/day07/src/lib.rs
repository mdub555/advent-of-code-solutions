use lazy_static::lazy_static;
use regex::Regex;
use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::rc::Rc;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
enum Source {
    Value(usize),
    Label(String),
}

impl Source {
    fn new(s: &str) -> Source {
        match s.parse::<usize>() {
            Ok(x) => Source::Value(x),
            Err(_) => Source::Label(s.to_string()),
        }
    }
}

#[derive(Debug)]
enum Op {
    And,
    Or,
    Not,
    LShift,
    RShift,
    Raw,
}

#[derive(Debug)]
struct Node {
    label: String,
    sources: Vec<Source>,
    data: Option<usize>,
    op: Op,
}

impl Node {
    fn new(line: &str) -> Node {
        lazy_static! {
            static ref MAIN_RE: Regex = Regex::new(r"^(.*) -> ([a-z]+)$").unwrap();
            static ref AND_RE: Regex = Regex::new(r"^([a-z0-9]+) AND ([a-z0-9]+)$").unwrap();
            static ref OR_RE: Regex = Regex::new(r"^([a-z0-9]+) OR ([a-z0-9]+)$").unwrap();
            static ref NOT_RE: Regex = Regex::new(r"^NOT ([a-z0-9]+)$").unwrap();
            static ref RAW_RE: Regex = Regex::new(r"^([a-z0-9]+)$").unwrap();
            static ref RS_RE: Regex = Regex::new(r"^([a-z0-9]+) RSHIFT ([0-9]+)$").unwrap();
            static ref LS_RE: Regex = Regex::new(r"^([a-z0-9]+) LSHIFT ([0-9]+)$").unwrap();
        }
        let caps = MAIN_RE.captures(line).unwrap();
        let label = caps.get(2).map_or("", |m| m.as_str());
        let mut sources: Vec<Source> = Vec::new();
        let mut op = Op::Raw;
        if label.is_empty() {
            panic!("No label parsed.");
        }
        let input = caps.get(1).map_or("", |m| m.as_str());

        if AND_RE.is_match(&input) {
            op = Op::And;
            let caps = AND_RE.captures(&input).unwrap();
            sources.push(Source::new(caps.get(1).unwrap().as_str()));
            sources.push(Source::new(caps.get(2).unwrap().as_str()));
        } else if OR_RE.is_match(&input) {
            op = Op::Or;
            let caps = OR_RE.captures(&input).unwrap();
            sources.push(Source::new(caps.get(1).unwrap().as_str()));
            sources.push(Source::new(caps.get(2).unwrap().as_str()));
        } else if NOT_RE.is_match(&input) {
            op = Op::Not;
            let caps = NOT_RE.captures(&input).unwrap();
            sources.push(Source::new(caps.get(1).unwrap().as_str()));
        } else if LS_RE.is_match(&input) {
            op = Op::LShift;
            let caps = LS_RE.captures(&input).unwrap();
            sources.push(Source::new(caps.get(1).unwrap().as_str()));
            sources.push(Source::new(caps.get(2).unwrap().as_str()));
        } else if RS_RE.is_match(&input) {
            op = Op::RShift;
            let caps = RS_RE.captures(&input).unwrap();
            sources.push(Source::new(caps.get(1).unwrap().as_str()));
            sources.push(Source::new(caps.get(2).unwrap().as_str()));
        } else if RAW_RE.is_match(&input) {
            let caps = RAW_RE.captures(&input).unwrap();
            sources.push(Source::new(caps.get(1).unwrap().as_str()));
        } else {
            panic!("Unhandled node: {}", &input);
        }
        Node {
            label: label.to_string(),
            sources: sources,
            data: None,
            op: op,
        }
    }

    fn get_data(&mut self, arena: &Arena) -> usize {
        if let Some(d) = self.data {
            return d;
        }
        let source_vals: Vec<usize> = self
            .sources
            .iter()
            .map(|source| match source {
                Source::Value(v) => *v,
                Source::Label(l) => {
                    let node: &mut Node = &mut arena.nodes.get(l).unwrap().borrow_mut();
                    node.get_data(&arena)
                }
            })
            .collect();

        let data = match self.op {
            Op::Raw => *source_vals.get(0).unwrap(),
            Op::And => *source_vals.get(0).unwrap() & *source_vals.get(1).unwrap(),
            Op::Or => *source_vals.get(0).unwrap() | *source_vals.get(1).unwrap(),
            Op::Not => (!*source_vals.get(0).unwrap()) & 0xFFFF,
            Op::LShift => (*source_vals.get(0).unwrap() << *source_vals.get(1).unwrap()) & 0xFFFF,
            Op::RShift => *source_vals.get(0).unwrap() >> *source_vals.get(1).unwrap(),
        };
        self.data = Some(data);
        data
    }
}

struct Arena {
    nodes: HashMap<String, Rc<RefCell<Node>>>,
}

impl Arena {
    fn new() -> Arena {
        Arena {
            nodes: HashMap::new(),
        }
    }

    fn add(&mut self, node: Node) {
        let label = node.label.to_string();
        if self.nodes.contains_key(&label) {
            panic!();
        }
        self.nodes.insert(label, Rc::new(RefCell::new(node)));
    }
}

fn part1(contents: &str) {
    let mut arena = Arena::new();
    let lines: Vec<&str> = contents.split("\n").collect();
    for line in lines {
        arena.add(Node::new(line));
    }
    let node: &mut Node = &mut arena.nodes.get("a").unwrap().borrow_mut();
    println!("{}", &node.get_data(&arena));
}

fn part2(contents: &str) {
    let mut arena = Arena::new();
    let lines: Vec<&str> = contents.split("\n").collect();
    for line in lines {
        arena.add(Node::new(line));
    }
    let a: usize;
    {
        let nodea: &mut Node = &mut arena.nodes.get("a").unwrap().borrow_mut();
        a = nodea.get_data(&arena);
    }
    for (_, node) in &arena.nodes {
        node.borrow_mut().data = None;
    }
    arena.nodes.get("b").unwrap().borrow_mut().data = Some(a);
    let node: &mut Node = &mut arena.nodes.get("a").unwrap().borrow_mut();
    println!("{}", &node.get_data(&arena));
}
