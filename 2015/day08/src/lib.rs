use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn escape(line: &str) -> String {
    let mut result = line.replace("\\\\", "a");
    result = result.replace("\\\"", "b");
    while let Some(i) = result.find("\\x") {
        result.replace_range(i..i + 4, "c");
    }
    result
}

fn code_len(line: &str) -> usize {
    let escaped = escape(line);
    escaped.len() - 2
}

fn encode(line: &str) -> String {
    let mut result = line.replace("\\", "\\\\");
    result = result.replace("\"", "\\\"");
    result
}

fn encode_len(line: &str) -> usize {
    let encoded = encode(line);
    encoded.len() + 2
}

fn part1(contents: &str) {
    let total: usize = contents
        .lines()
        .map(|line| line.len() - code_len(&line))
        .sum();
    println!("Size difference: {}", total);
}

fn part2(contents: &str) {
    let total: usize = contents
        .lines()
        .map(|line| encode_len(&line) - line.len())
        .sum();
    println!("Size difference: {}", total);
}
