use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

struct Item {
    cost: u32,
    damage: u32,
    armor: u32,
}

impl Item {
    fn new(cost: u32, damage: u32, armor: u32) -> Item {
        Item {
            cost,
            damage,
            armor,
        }
    }
}

struct Character {
    hp: u32,
    damage: u32,
    armor: u32,
}

impl Character {
    fn wins(&self, other: &Character) -> bool {
        let my_damage_per_turn: u32 = if self.damage > other.armor {
            self.damage - other.armor
        } else {
            1
        };
        let mut turns_to_kill = other.hp / my_damage_per_turn;
        if other.hp % my_damage_per_turn > 0 {
            turns_to_kill += 1;
        }
        let other_damage_per_turn: u32 = if other.damage > self.armor {
            other.damage - self.armor
        } else {
            1
        };
        let mut turns_to_die = self.hp / other_damage_per_turn;
        if self.hp % other_damage_per_turn > 0 {
            turns_to_die += 1;
        }
        return turns_to_kill <= turns_to_die;
    }

    fn add(&mut self, item: &Item) {
        self.damage += item.damage;
        self.armor += item.armor;
    }

    fn clear(&mut self) {
        self.damage = 0;
        self.armor = 0;
    }
}

fn equip(c: &mut Character, items: &Vec<Item>, index: usize) -> u32 {
    if let Some(item) = items.get(index) {
        c.add(item);
        return item.cost;
    }
    0
}

fn part1(_contents: &str) {
    let boss = Character {
        hp: 103,
        damage: 9,
        armor: 2,
    };
    let mut player = Character {
        hp: 100,
        damage: 0,
        armor: 0,
    };
    let weapons = vec![
        Item::new(8, 4, 0),
        Item::new(10, 5, 0),
        Item::new(25, 6, 0),
        Item::new(40, 7, 0),
        Item::new(74, 8, 0),
    ];
    let armors = vec![
        Item::new(13, 0, 1),
        Item::new(31, 0, 2),
        Item::new(53, 0, 3),
        Item::new(75, 0, 4),
        Item::new(102, 0, 5),
    ];
    let rings = vec![
        Item::new(25, 1, 0),
        Item::new(50, 2, 0),
        Item::new(100, 3, 0),
        Item::new(20, 0, 1),
        Item::new(40, 0, 2),
        Item::new(80, 0, 3),
    ];
    let mut best = u32::MAX;
    let mut setup = (0, 0, 0, 0);
    for w in 0..weapons.len() {
        for a in 0..armors.len() + 1 {
            for r1 in 0..rings.len() + 1 {
                for r2 in 0..rings.len() + 1 {
                    if r1 == r2 && r1 < rings.len() {
                        continue;
                    }
                    player.clear();
                    let mut cost = 0;
                    cost += equip(&mut player, &weapons, w);
                    cost += equip(&mut player, &armors, a);
                    cost += equip(&mut player, &rings, r1);
                    cost += equip(&mut player, &rings, r2);
                    if cost < best && player.wins(&boss) {
                        best = cost;
                        setup = (w, a, r1, r2);
                    }
                }
            }
        }
    }
    println!("Cost: {}. Setup: {:?}", best, setup);
}

fn part2(_contents: &str) {
    let boss = Character {
        hp: 103,
        damage: 9,
        armor: 2,
    };
    let mut player = Character {
        hp: 100,
        damage: 0,
        armor: 0,
    };
    let weapons = vec![
        Item::new(8, 4, 0),
        Item::new(10, 5, 0),
        Item::new(25, 6, 0),
        Item::new(40, 7, 0),
        Item::new(74, 8, 0),
    ];
    let armors = vec![
        Item::new(13, 0, 1),
        Item::new(31, 0, 2),
        Item::new(53, 0, 3),
        Item::new(75, 0, 4),
        Item::new(102, 0, 5),
    ];
    let rings = vec![
        Item::new(25, 1, 0),
        Item::new(50, 2, 0),
        Item::new(100, 3, 0),
        Item::new(20, 0, 1),
        Item::new(40, 0, 2),
        Item::new(80, 0, 3),
    ];
    let mut best = 0;
    let mut setup = (0, 0, 0, 0);
    for w in 0..weapons.len() {
        for a in 0..armors.len() + 1 {
            for r1 in 0..rings.len() + 1 {
                for r2 in 0..rings.len() + 1 {
                    if r1 == r2 && r1 < rings.len() {
                        continue;
                    }
                    player.clear();
                    let mut cost = 0;
                    cost += equip(&mut player, &weapons, w);
                    cost += equip(&mut player, &armors, a);
                    cost += equip(&mut player, &rings, r1);
                    cost += equip(&mut player, &rings, r2);
                    if cost > best && !player.wins(&boss) {
                        best = cost;
                        setup = (w, a, r1, r2);
                    }
                }
            }
        }
    }
    println!("Cost: {}. Setup: {:?}", best, setup);
}
