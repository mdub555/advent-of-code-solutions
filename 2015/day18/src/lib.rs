use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Grid {
    grid: Vec<Vec<bool>>,
}

impl std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for row in &self.grid {
            for cell in row {
                if *cell {
                    if let Err(e) = write!(f, "#") {
                        return Err(e);
                    }
                } else {
                    if let Err(e) = write!(f, ".") {
                        return Err(e);
                    }
                }
            }
            if let Err(e) = write!(f, "\n") {
                return Err(e);
            }
        }
        Ok(())
    }
}

impl Grid {
    fn new(init: &str) -> Grid {
        let mut grid = Vec::new();
        for line in init.lines() {
            let mut row: Vec<bool> = Vec::new();
            for c in line.chars() {
                match c {
                    '#' => row.push(true),
                    '.' => row.push(false),
                    _ => panic!("Unknown character: {}", c),
                }
            }
            grid.push(row);
        }
        Grid { grid }
    }

    fn step(&mut self) {
        let mut next = self.grid.clone();
        for r in 0..self.grid.len() {
            for c in 0..self.grid[r].len() {
                next[r][c] = self.get_next(r, c);
            }
        }
        self.grid = next;
    }

    fn step2(&mut self) {
        let width = self.grid[0].len() - 1;
        let height = self.grid.len() - 1;
        self.grid[0][0] = true;
        self.grid[0][width] = true;
        self.grid[height][0] = true;
        self.grid[height][width] = true;

        let mut next = self.grid.clone();
        for r in 0..self.grid.len() {
            for c in 0..self.grid[r].len() {
                next[r][c] = self.get_next(r, c);
            }
        }
        self.grid = next;

        self.grid[0][0] = true;
        self.grid[0][width] = true;
        self.grid[height][0] = true;
        self.grid[height][width] = true;
    }

    fn get_next(&self, row: usize, col: usize) -> bool {
        let mut on_neighbors: usize = 0;
        for r in -1..2 {
            for c in -1..2 {
                if r == 0 && c == 0 {
                    continue;
                }
                if self.get(row as isize + r, col as isize + c) {
                    on_neighbors += 1;
                }
            }
        }
        if self.grid[row][col] {
            return on_neighbors == 2 || on_neighbors == 3;
        } else {
            return on_neighbors == 3;
        }
    }

    fn get(&self, row: isize, col: isize) -> bool {
        if row < 0 || row as usize >= self.grid.len() {
            return false;
        }
        if col < 0 || col as usize >= self.grid[row as usize].len() {
            return false;
        }
        return self.grid[row as usize][col as usize];
    }

    fn num_on(&self) -> usize {
        let mut total = 0;
        for row in &self.grid {
            for cell in row {
                if *cell {
                    total += 1;
                }
            }
        }
        total
    }
}

fn part1(contents: &str) {
    let mut grid = Grid::new(contents);
    for _ in 0..100 {
        grid.step();
    }
    println!("{} Lights on", grid.num_on());
}

fn part2(contents: &str) {
    let mut grid = Grid::new(contents);
    for _ in 0..100 {
        grid.step2();
    }
    println!("{} Lights on", grid.num_on());
}
