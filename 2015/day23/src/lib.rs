use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn process(instructions: &Vec<String>, a: &mut u32, b: &mut u32) {
    let mut idx: usize = 0;
    loop {
        if idx >= instructions.len() {
            return;
        }
        let ins: Vec<&str> = instructions[idx].split(' ').collect();
        match ins[0] {
            "tpl" => {
                idx += 1;
                match ins[1] {
                    "a" => *a *= 3,
                    "b" => *b *= 3,
                    _ => panic!("unknown register {}", ins[1]),
                };
            }
            "hlf" => {
                idx += 1;
                match ins[1] {
                    "a" => *a /= 2,
                    "b" => *b /= 2,
                    _ => panic!("unknown register {}", ins[1]),
                };
            }
            "inc" => {
                idx += 1;
                match ins[1] {
                    "a" => *a += 1,
                    "b" => *b += 1,
                    _ => panic!("unknown register {}", ins[1]),
                };
            }
            "jmp" => {
                let diff: isize = ins[1].parse().unwrap();
                if idx as isize + diff < 0 {
                    return;
                }
                idx = (idx as isize + diff) as usize;
            }
            "jie" => {
                match ins[1] {
                    "a," => {
                        if *a % 2 != 0 {
                            idx += 1;
                            continue;
                        }
                    }
                    "b," => {
                        if *b % 2 != 0 {
                            idx += 1;
                            continue;
                        }
                    }
                    _ => panic!("unknown register {}", ins[1]),
                };
                let diff: isize = ins[2].parse().unwrap();
                if idx as isize + diff < 0 {
                    return;
                }
                idx = (idx as isize + diff) as usize;
            }
            "jio" => {
                match ins[1] {
                    "a," => {
                        if *a != 1 {
                            idx += 1;
                            continue;
                        }
                    }
                    "b," => {
                        if *b != 1 {
                            idx += 1;
                            continue;
                        }
                    }
                    _ => panic!("unknown register {}", ins[1]),
                };
                let diff: isize = ins[2].parse().unwrap();
                if idx as isize + diff < 0 {
                    return;
                }
                idx = (idx as isize + diff) as usize;
            }
            _ => panic!("unknown instruction {}", ins[0]),
        }
    }
}

fn part1(contents: &str) {
    let instructions: Vec<String> = contents.lines().map(|l| l.to_string()).collect();
    let mut a = 0;
    let mut b = 0;
    process(&instructions, &mut a, &mut b);
    println!("b = {}", b);
}

fn part2(contents: &str) {
    let instructions: Vec<String> = contents.lines().map(|l| l.to_string()).collect();
    let mut a = 1;
    let mut b = 0;
    process(&instructions, &mut a, &mut b);
    println!("b = {}", b);
}
