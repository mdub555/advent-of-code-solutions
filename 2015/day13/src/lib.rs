use itertools::Itertools;
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

struct Relationship {
    person1: String,
    person2: String,
    value: isize,
}

impl Relationship {
    fn parse(line: &str) -> Relationship {
        let re = Regex::new("^([a-zA-Z]+) would ([a-zA-Z]+) ([0-9]+) happiness units by sitting next to ([a-zA-Z]+).$").unwrap();
        let caps = re.captures(line).unwrap();
        let person1 = caps.get(1).unwrap().as_str().to_string();
        let positive = caps.get(2).unwrap().as_str() == "gain";
        let tmp: isize = caps.get(3).unwrap().as_str().parse().unwrap();
        let person2 = caps.get(4).unwrap().as_str().to_string();
        let value: isize;
        if positive {
            value = tmp;
        } else {
            value = -tmp;
        }

        Relationship {
            person1,
            person2,
            value,
        }
    }
}

#[derive(Debug)]
struct Chart {
    people: HashMap<String, usize>,
    happiness: Vec<Vec<isize>>,
}

impl Chart {
    fn new() -> Chart {
        Chart {
            people: HashMap::new(),
            happiness: Vec::new(),
        }
    }

    fn add_person(&mut self, name: &str) {
        if !self.people.contains_key(name) {
            let num_people = self.people.len();
            self.people.insert(name.to_string().clone(), num_people);
            for happy in &mut self.happiness {
                happy.push(0);
            }
            self.happiness.push(vec![0; num_people + 1]);
        }
    }

    fn add(&mut self, relationship: &Relationship) {
        self.add_person(&relationship.person1);
        self.add_person(&relationship.person2);

        let i1: usize = *self.people.get(&relationship.person1).unwrap();
        let i2: usize = *self.people.get(&relationship.person2).unwrap();
        self.happiness[i1][i2] = relationship.value;
    }

    fn total_happiness(&self, order: &Vec<&usize>) -> isize {
        let mut total = 0;
        for i in 0..order.len() - 1 {
            total += self.happiness[*order[i]][*order[i + 1]];
            total += self.happiness[*order[i + 1]][*order[i]];
        }
        total += self.happiness[*order[order.len() - 1]][*order[0]];
        total += self.happiness[*order[0]][*order[order.len() - 1]];
        total
    }

    fn max_happiness(&self) -> isize {
        let mut order: Vec<usize> = Vec::new();
        for i in 0..self.people.len() {
            order.push(i);
        }
        let mut max = isize::MIN;
        for perm in order.iter().permutations(order.len()) {
            let happiness = self.total_happiness(&perm);
            if happiness > max {
                max = happiness;
            }
        }
        max
    }
}

fn part1(contents: &str) {
    let mut chart = Chart::new();
    for line in contents.lines() {
        let r = Relationship::parse(line);
        chart.add(&r);
    }
    println!("Max happiness: {}", chart.max_happiness());
}

fn part2(contents: &str) {
    let mut chart = Chart::new();
    for line in contents.lines() {
        let r = Relationship::parse(line);
        chart.add(&r);
    }
    chart.add_person("me");
    println!("Max happiness with me: {}", chart.max_happiness());
}
