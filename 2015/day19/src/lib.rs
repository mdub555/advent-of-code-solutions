use std::collections::HashMap;
use std::collections::HashSet;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn parse_contents(contents: &str) -> (String, HashMap<String, Vec<String>>) {
    let mut molecule = String::from("");
    let mut mutations: HashMap<String, Vec<String>> = HashMap::new();
    let mut processing_mutations = true;
    for line in contents.lines() {
        if line.is_empty() {
            processing_mutations = false;
            continue;
        }
        if processing_mutations {
            let a: Vec<&str> = line.split(" => ").collect();
            let m = mutations.entry(a[0].to_string()).or_insert(Vec::new());
            m.push(a[1].to_string());
        } else {
            molecule = line.to_string();
        }
    }
    (molecule, mutations)
}

fn do_replacements(molecule: &str, mutations: &HashMap<String, Vec<String>>) -> HashSet<String> {
    let mut molecules = HashSet::new();
    for (mutation, replacements) in mutations {
        for replacement in replacements {
            for (i, _) in molecule.match_indices(mutation) {
                let mut new_molecule = molecule.to_string();
                new_molecule.replace_range(i..i + mutation.len(), replacement);
                molecules.insert(new_molecule);
            }
        }
    }
    molecules
}

fn reduce_all(
    molecules: &HashSet<String>,
    mutations: &HashMap<String, Vec<String>>,
) -> HashSet<String> {
    let mut all_reduced = HashSet::new();
    for molecule in molecules {
        let reduced = reduce(molecule, mutations);
        for r in reduced {
            all_reduced.insert(r);
        }
    }
    all_reduced
}

fn reduce(molecule: &str, mutations: &HashMap<String, Vec<String>>) -> HashSet<String> {
    let mut molecules = HashSet::new();
    for (mutation, replacements) in mutations {
        for replacement in replacements {
            for (i, _) in molecule.match_indices(mutation) {
                let mut new_molecule = molecule.to_string();
                new_molecule.replace_range(i..i + mutation.len(), replacement);
                if new_molecule.len() == 1 || !new_molecule.contains("e") {
                    molecules.insert(new_molecule);
                }
            }
        }
    }
    molecules
}

fn part1(contents: &str) {
    let (molecule, mutations) = parse_contents(&contents);
    let molecules = do_replacements(&molecule, &mutations);

    println!("{} different molecules", molecules.len());
}

fn part2(contents: &str) {
    let (molecule, mutations) = parse_contents(&contents);
    let mut molecules: HashSet<String> = HashSet::new();
    let mut reverse_mutations: HashMap<String, Vec<String>> = HashMap::new();
    for (mutation, replacements) in &mutations {
        for replacement in replacements {
            let m = reverse_mutations
                .entry(replacement.to_string())
                .or_insert(Vec::new());
            m.push(mutation.to_string());
        }
    }
    molecules.insert(molecule.clone());
    let mut step: u32 = 0;
    while !molecules.contains("e") {
        molecules = reduce_all(&molecules, &reverse_mutations);
        step += 1;
        println!("{}, {}", step, molecules.len());
    }
    println!("Molecule can be made after {} steps.", step);
}
