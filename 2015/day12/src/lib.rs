use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn sum(value: &json::JsonValue) -> i32 {
    match value {
        json::JsonValue::Null => 0,
        json::JsonValue::Short(_) => 0,
        json::JsonValue::String(_) => 0,
        json::JsonValue::Number(_) => value.as_i32().unwrap(),
        json::JsonValue::Boolean(_) => 0,
        json::JsonValue::Object(_) => {
            let mut total = 0;
            for (_, v) in value.entries() {
                total += sum(v);
            }
            total
        }
        json::JsonValue::Array(_) => {
            let mut total = 0;
            for v in value.members() {
                total += sum(v);
            }
            total
        }
    }
}

fn sum_non_red(value: &json::JsonValue) -> i32 {
    match value {
        json::JsonValue::Null => 0,
        json::JsonValue::Short(_) => 0,
        json::JsonValue::String(_) => 0,
        json::JsonValue::Number(_) => value.as_i32().unwrap(),
        json::JsonValue::Boolean(_) => 0,
        json::JsonValue::Object(_) => {
            let mut total = 0;
            for (_, v) in value.entries() {
                match v {
                    json::JsonValue::String(s) => {
                        if s == "red" {
                            return 0;
                        }
                    }
                    json::JsonValue::Short(s) => {
                        if s == "red" {
                            return 0;
                        }
                    }
                    _ => (),
                }
                total += sum_non_red(v);
            }
            total
        }
        json::JsonValue::Array(_) => {
            let mut total = 0;
            for v in value.members() {
                total += sum_non_red(v);
            }
            total
        }
    }
}

fn part1(contents: &str) {
    let parsed = json::parse(contents).unwrap();
    println!("Total: {}", sum(&parsed));
}

fn part2(contents: &str) {
    let parsed = json::parse(contents).unwrap();
    println!("Non-red: {}", sum_non_red(&parsed));
}
