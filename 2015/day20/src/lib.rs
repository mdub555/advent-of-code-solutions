use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn presents(house: u32) -> u32 {
    let mut total = 0;
    let sqrt: f32 = (house as f32).sqrt();
    for i in 1..sqrt as u32 + 1 {
        if house % i == 0 {
            total += i;
            total += house / i;
        }
    }
    if sqrt == (sqrt as u32) as f32 {
        total -= sqrt as u32;
    }
    total
}

// Only accurate for house > 50^2.
fn presents2(house: u32) -> u32 {
    let mut total = 0;
    for i in 1..51 {
        if house % i == 0 {
            total += house / i;
        }
    }
    total
}

fn part1(contents: &str) {
    let desired: u32 = contents.parse().unwrap();
    let mut house: u32 = 1;
    while presents(house) < desired / 10 {
        if house % 100000 == 0 {
            println!("House: {}", house);
        }
        house += 1;
    }
    println!("House {}: {} presents", house, presents(house));
}

fn part2(contents: &str) {
    let desired: u32 = contents.parse().unwrap();
    let mut house: u32 = 5000;
    while presents2(house) < desired / 11 {
        if house % 100000 == 0 {
            println!("House: {}, {}", house, presents2(house));
        }
        house += 1;
    }
    println!("House {}: {} presents", house, presents2(house));
}
