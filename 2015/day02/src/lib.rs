use std::cmp;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

struct Present {
    height: u32,
    width: u32,
    length: u32,
}

impl Present {
    fn new(line: &str) -> Present {
        let dims: Vec<&str> = line.split("x").collect();
        Present {
            height: dims[0].parse().unwrap(),
            width: dims[1].parse().unwrap(),
            length: dims[2].parse().unwrap(),
        }
    }

    fn surface_area(&self) -> u32 {
        return 2 * self.length * self.width
            + 2 * self.width * self.height
            + 2 * self.height * self.length;
    }

    fn smallest_area(&self) -> u32 {
        let a1 = self.length * self.width;
        let a2 = self.width * self.height;
        let a3 = self.height * self.length;
        return cmp::min(a1, cmp::min(a2, a3));
    }

    fn volume(&self) -> u32 {
        return self.length * self.width * self.height;
    }

    fn smallest_perimeter(&self) -> u32 {
        let max = cmp::max(self.length, cmp::max(self.width, self.height));
        return (self.length + self.width + self.height - max) * 2;
    }
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn part1(contents: &str) {
    let presents = contents.split("\n").map(|line| Present::new(line));
    let mut sum = 0;
    for p in presents {
        sum += p.surface_area() + p.smallest_area();
    }
    println!("all wrapping paper: {}", sum);
}

fn part2(contents: &str) {
    let presents = contents.split("\n").map(|line| Present::new(line));
    let mut sum = 0;
    for p in presents {
        sum += p.volume() + p.smallest_perimeter();
    }
    println!("all ribbon: {}", sum);
}
