use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
enum Instruction {
    Noop,
    Addx(isize),
}

struct Cpu {
    reg: isize, // register
    prog: Vec<Instruction>,
    pc: usize, // Program counter
    adding: bool,
}

impl Cpu {
    fn new(prog: Vec<Instruction>) -> Cpu {
        Cpu {
            reg: 1,
            prog,
            pc: 0,
            adding: false,
        }
    }

    fn tick(&mut self) -> isize {
        let tmp = self.reg;
        let ins = &self.prog[self.pc];
        match ins {
            Instruction::Noop => self.pc += 1,
            Instruction::Addx(x) => {
                if self.adding {
                    self.reg += x;
                    self.adding = false;
                    self.pc += 1;
                } else {
                    self.adding = true;
                }
            }
        }
        return tmp;
    }
}

struct Crt {
    index: usize,
    buffer: Vec<bool>,
    width: usize,
}

impl Crt {
    fn new() -> Crt {
        Crt {
            index: 0,
            width: 40,
            buffer: vec![],
        }
    }

    fn tick(&mut self, sprite_pos: isize) {
        let col: isize = (self.index % self.width) as isize;
        self.buffer.push((sprite_pos - col).abs() <= 1);
        self.index += 1;
    }

    fn draw(&self) {
        for i in 0..self.buffer.len() {
            if i % self.width == 0 {
                println!();
            }
            if self.buffer[i] {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
}

fn parse(contents: &str) -> Vec<Instruction> {
    let mut program: Vec<Instruction> = vec![];
    for line in contents.lines() {
        let mut tokens = line.split(' ');
        let ins = match tokens.next() {
            Some("noop") => Instruction::Noop,
            Some("addx") => Instruction::Addx(tokens.next().unwrap().parse().unwrap()),
            Some(x) => panic!("Unknown instruction: {}", x),
            None => panic!("None instruction found"),
        };
        program.push(ins);
    }
    return program;
}

fn part1(contents: &str) {
    let mut cpu = Cpu::new(parse(&contents));
    let mut total: isize = 0;
    for i in 1..221 {
        let x = cpu.tick();
        if (i - 20) % 40 == 0 {
            let strength: isize = i * x;
            total += strength;
        }
    }
    println!("Part 1: {}", total);
}

fn part2(contents: &str) {
    let mut cpu = Cpu::new(parse(&contents));
    let mut crt = Crt::new();
    for _ in 0..240 {
        let x = cpu.tick();
        crt.tick(x);
    }
    print!("Part 2:");
    crt.draw();
}
