use std::cmp::min;
use std::collections::VecDeque;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Cell {
    val: usize,
    letter: char,
    visited: bool,
    steps: usize,
}

impl Cell {
    fn new(c: char) -> Cell {
        Cell {
            val: match c {
                'S' => 'a' as usize - 97,
                'E' => 'z' as usize - 97,
                x => x as usize - 97,
            },
            letter: c,
            visited: false,
            steps: 0,
        }
    }
}

// Helper to find all cells of a given label.
fn find_all(grid: &Vec<Vec<Cell>>, letter: char) -> Vec<(usize, usize)> {
    let mut tmp = vec![];
    for r in 0..grid.len() {
        for c in 0..grid[r].len() {
            if grid[r][c].letter == letter {
                tmp.push((r, c));
            }
        }
    }
    return tmp;
}

fn parse(contents: &str) -> Vec<Vec<Cell>> {
    let mut grid = vec![];
    for line in contents.lines() {
        let mut row = vec![];
        for c in line.chars() {
            row.push(Cell::new(c));
        }
        grid.push(row);
    }
    return grid;
}

// The initial problem is to get from start to end, but instead I'll
// go from end to start. This makes part 2 easier.
fn reverse_bfs(grid: &mut Vec<Vec<Cell>>, end: (usize, usize)) {
    let mut q = VecDeque::new();
    grid[end.0][end.1].visited = true;
    q.push_back(end);
    while !q.is_empty() {
        let (row, col) = q.pop_front().unwrap();
        let step = grid[row][col].steps;
        let height = grid[row][col].val;
        let neighbors: Vec<(isize, isize)> = vec![
            (row as isize + 1, col as isize),
            (row as isize - 1, col as isize),
            (row as isize, col as isize + 1),
            (row as isize, col as isize - 1),
        ];
        for n in neighbors {
            if n.0 < 0 || n.1 < 0 || n.0 >= grid.len() as isize || n.1 >= grid[0].len() as isize {
                continue;
            }
            let cell = &mut grid[n.0 as usize][n.1 as usize];
            if cell.val + 1 < height || cell.visited {
                continue;
            }
            cell.visited = true;
            cell.steps = step + 1;
            q.push_back((n.0 as usize, n.1 as usize));
        }
    }
}

fn part1(contents: &str) {
    let mut grid = parse(contents);
    let end = find_all(&grid, 'E')[0];
    reverse_bfs(&mut grid, end);
    let start = find_all(&grid, 'S')[0];
    println!("Part 1: path length is {}", &grid[start.0][start.1].steps);
}

fn part2(contents: &str) {
    let mut grid = parse(contents);
    let end = find_all(&grid, 'E')[0];
    reverse_bfs(&mut grid, end);
    let mut all_as = find_all(&grid, 'a');
    all_as.push(find_all(&grid, 'S')[0]);
    let mut shortest = 1000000;
    for a in &all_as {
        if grid[a.0][a.1].steps == 0 {
            continue;
        }
        shortest = min(shortest, grid[a.0][a.1].steps);
    }
    println!("Part 2: shortest path length {}", shortest);
}
