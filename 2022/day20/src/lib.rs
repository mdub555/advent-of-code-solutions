use std::cell::RefCell;
use std::error::Error;
use std::fmt;
use std::fs;
use std::rc::Rc;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct LL {
    list: Vec<Node>,
    head: Option<usize>,
}

impl fmt::Display for LL {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[");
        if let Some(head) = self.head {
            let mut node = &self.list[head];
            loop {
                write!(f, "{}, ", node.val);
                match node.next {
                    None => break,
                    Some(i) => {
                        if i == head {
                            break;
                        }
                        node = &self.list[i];
                    }
                }
            }
        }
        write!(f, "]")
    }
}

impl LL {
    fn new() -> LL {
        LL {
            list: vec![],
            head: None,
        }
    }

    fn from(vec: &Vec<isize>) -> LL {
        let mut ll = LL::new();
        ll.head = Some(0);
        for i in 0..vec.len() {
            let mut node = Node::new(vec[i]);
            if i == 0 {
                node.prev = Some(vec.len() - 1);
            } else {
                node.prev = Some(i - 1);
            }
            if i == vec.len() - 1 {
                node.next = Some(0);
            } else {
                node.next = Some(i + 1);
            }
            ll.list.push(node);
        }
        return ll;
    }

    //fn mix(&mut self, index: usize) {
    //    let n = &self.list[index];
    //    // remove
    //    if let Some(prev) = n.prev {
    //        self.list[prev].next = n.next;
    //    }
    //    if let Some(next) = n.next {
    //        self.list[next].prev = n.prev;
    //    }
    //}
}

#[derive(Debug)]
struct Node {
    val: isize,
    next: Option<usize>,
    prev: Option<usize>,
}

impl Node {
    fn new(val: isize) -> Node {
        Node {
            val,
            next: None,
            prev: None,
        }
    }
}

fn part1(contents: &str) {
    let numbers: Vec<isize> = contents.lines().map(|l| l.parse().unwrap()).collect();
    let ll = LL::from(&numbers);

    println!("{:?}", numbers);
    println!("{}", ll);
}

fn part2(contents: &str) {}
