use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Move {
    amount: usize,
    src: usize,
    dst: usize,
}

fn parse_line(line: &str) -> Move {
    let tokens: Vec<&str> = line.split(' ').collect();
    Move {
        amount: tokens[1].parse().unwrap(),
        src: tokens[3].parse::<usize>().unwrap() - 1,
        dst: tokens[5].parse::<usize>().unwrap() - 1,
    }
}

fn parse_moves(contents: &str) -> Vec<Move> {
    let mut moves: Vec<Move> = vec![];
    let mut ignoring = true;
    for line in contents.lines() {
        if ignoring && line.is_empty() {
            ignoring = false;
            continue;
        }
        if ignoring {
            continue;
        }
        moves.push(parse_line(line));
    }
    moves
}

fn init_test() -> Vec<Vec<char>> {
    let stack0: Vec<char> = vec!['Z', 'N'];
    let stack1: Vec<char> = vec!['M', 'C', 'D'];
    let stack2: Vec<char> = vec!['P'];
    vec![stack0, stack1, stack2]
}

fn init_full() -> Vec<Vec<char>> {
    let stack0: Vec<char> = vec!['Z', 'P', 'M', 'H', 'R'];
    let stack1: Vec<char> = vec!['P', 'C', 'J', 'B'];
    let stack2: Vec<char> = vec!['S', 'N', 'H', 'G', 'L', 'C', 'D'];
    let stack3: Vec<char> = vec!['F', 'T', 'M', 'D', 'Q', 'S', 'R', 'L'];
    let stack4: Vec<char> = vec!['F', 'S', 'P', 'Q', 'B', 'T', 'Z', 'M'];
    let stack5: Vec<char> = vec!['T', 'F', 'S', 'Z', 'B', 'G'];
    let stack6: Vec<char> = vec!['N', 'R', 'V'];
    let stack7: Vec<char> = vec!['P', 'G', 'L', 'T', 'D', 'V', 'C', 'M'];
    let stack8: Vec<char> = vec!['W', 'Q', 'N', 'J', 'F', 'M', 'L'];
    vec![
        stack0, stack1, stack2, stack3, stack4, stack5, stack6, stack7, stack8,
    ]
}

fn apply1(stacks: &mut Vec<Vec<char>>, m: &Move) {
    for _ in 0..m.amount {
        if let Some(tmp) = stacks[m.src].pop() {
            stacks[m.dst].push(tmp);
        }
    }
}

fn apply2(stacks: &mut Vec<Vec<char>>, m: &Move) {
    let mut holder: Vec<char> = vec![];
    for _ in 0..m.amount {
        if let Some(tmp) = stacks[m.src].pop() {
            holder.push(tmp);
        }
    }
    for _ in 0..m.amount {
        if let Some(tmp) = holder.pop() {
            stacks[m.dst].push(tmp);
        }
    }
}

fn part1(contents: &str) {
    let mut stacks = init_full();
    let moves = parse_moves(contents);
    for m in &moves {
        apply1(&mut stacks, m);
    }
    println!("Part 1:");
    for i in 0..stacks.len() {
        if let Some(top) = stacks[i].last() {
            print!("{}", top);
        }
    }
    println!();
}

fn part2(contents: &str) {
    let mut stacks = init_full();
    let moves = parse_moves(contents);
    for m in &moves {
        apply2(&mut stacks, m);
    }
    println!("Part 1:");
    for i in 0..stacks.len() {
        if let Some(top) = stacks[i].last() {
            print!("{}", top);
        }
    }
    println!();
}
