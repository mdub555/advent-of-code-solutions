use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn parse_range(line: &str) -> (u32, u32) {
    let vals: Vec<u32> = line.split('-').map(|v| v.parse().unwrap()).collect();
    return (vals[0], vals[1]);
}

fn parse_line(line: &str) -> ((u32, u32), (u32, u32)) {
    let pairs: Vec<&str> = line.split(',').collect();
    return (parse_range(pairs[0]), parse_range(pairs[1]));
}

fn complete_overlap(range0: (u32, u32), range1: (u32, u32)) -> bool {
    return (range0.0 <= range1.0 && range0.1 >= range1.1)
        || (range1.0 <= range0.0 && range1.1 >= range0.1);
}

fn any_overlap(range0: (u32, u32), range1: (u32, u32)) -> bool {
    return !((range0.1 < range1.0) || (range1.1 < range0.0));
}

fn part1(contents: &str) {
    let mut overlaps = 0;
    for line in contents.lines() {
        let pairs = parse_line(line);
        if complete_overlap(pairs.0, pairs.1) {
            overlaps += 1;
        }
    }
    println!("Part 1: {} overlaps", overlaps);
}

fn part2(contents: &str) {
    let mut overlaps = 0;
    for line in contents.lines() {
        let pairs = parse_line(line);
        if any_overlap(pairs.0, pairs.1) {
            overlaps += 1;
        }
    }
    println!("Part 2: {} overlaps", overlaps);
}
