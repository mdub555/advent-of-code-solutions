use regex::Regex;
use std::collections::HashSet;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug, Eq, PartialEq, Hash)]
struct Location {
    x: isize,
    y: isize,
}

impl Location {
    fn manhattan(&self, other: &Location) -> usize {
        (self.x - other.x).abs() as usize + (self.y - other.y).abs() as usize
    }
}

#[derive(Debug)]
struct Sensor {
    loc: Location,
    beacon: Location,
}

impl Sensor {
    fn new(line: &str) -> Sensor {
        let re = Regex::new(r"^Sensor at x=(-?[0-9]+), y=(-?[0-9]+): closest beacon is at x=(-?[0-9]+), y=(-?[0-9]+)$")
            .unwrap();
        let caps = re.captures(line).unwrap();
        return Sensor {
            loc: Location {
                x: caps.get(1).unwrap().as_str().parse().unwrap(),
                y: caps.get(2).unwrap().as_str().parse().unwrap(),
            },
            beacon: Location {
                x: caps.get(3).unwrap().as_str().parse().unwrap(),
                y: caps.get(4).unwrap().as_str().parse().unwrap(),
            },
        };
    }

    fn dist_to_beacon(&self) -> usize {
        self.loc.manhattan(&self.beacon)
    }

    fn get_segment(&self, y: isize) -> Option<(isize, isize)> {
        let dist = self.dist_to_beacon();
        let dy = (self.loc.y - y).abs() as usize;
        if dy > dist {
            return None;
        }
        let dx = dist - dy;
        Some((self.loc.x - (dx as isize), self.loc.x + (dx as isize)))
    }
}

#[derive(Debug)]
struct Row {
    segments: Vec<(isize, isize)>,
}

impl Row {
    fn new() -> Row {
        Row { segments: vec![] }
    }

    fn num_occupied(&self) -> usize {
        let mut total: usize = 0;
        for s in &self.segments {
            total += (s.1 - s.0) as usize + 1;
        }
        return total;
    }

    fn add(&mut self, segment: (isize, isize)) {
        let mut i = 0;
        while i < self.segments.len() && self.segments[i].1 < segment.0 - 1 {
            i += 1;
        }
        // No segments this far, add to end
        if i >= self.segments.len() {
            self.segments.push(segment);
            return;
        }
        // No overlap with segment i, insert before
        if segment.1 < self.segments[i].0 - 1 {
            self.segments.insert(i, segment);
            return;
        }
        // Overlap with i
        if self.segments[i].0 > segment.0 {
            self.segments[i].0 = segment.0;
        }
        // Complete overlap, no addition
        if self.segments[i].1 >= segment.1 {
            return;
        }
        // Merge the two
        self.segments[i].1 = segment.1;
        // consume any other overlaps
        while i < self.segments.len() - 1 && self.segments[i].1 > self.segments[i + 1].1 {
            self.segments.remove(i + 1);
        }
        // merge with next segment
        if i < self.segments.len() - 1 && self.segments[i].1 >= self.segments[i + 1].0 {
            self.segments[i].1 = self.segments[i + 1].1;
            self.segments.remove(i + 1);
        }
    }

    fn trim(&mut self, maxi: isize) {
        if self.segments.is_empty() {
            return;
        }
        while !self.segments.is_empty() && self.segments[0].1 < 0 {
            self.segments.remove(0);
        }
        if self.segments.is_empty() {
            return;
        }
        if self.segments[0].0 < 0 {
            self.segments[0].0 = 0;
        }
        while !self.segments.is_empty() && self.segments[self.segments.len() - 1].0 > maxi {
            self.segments.remove(self.segments.len() - 1);
        }
        if self.segments.is_empty() {
            return;
        }
        let last = self.segments.len() - 1;
        if self.segments[last].1 > maxi {
            self.segments[last].1 = maxi;
        }
    }
}

fn part1(contents: &str) {
    let sensors: Vec<Sensor> = contents.lines().map(|l| Sensor::new(l)).collect();
    let y = 2000000;
    let mut row = Row::new();
    let mut beacons = HashSet::new();
    for sensor in &sensors {
        if sensor.beacon.y == y {
            beacons.insert(&sensor.beacon);
        }
        if let Some(segment) = sensor.get_segment(y) {
            row.add(segment);
        }
    }
    println!("Part 1: {}", row.num_occupied() - beacons.len());
}

fn part2(contents: &str) {
    let sensors: Vec<Sensor> = contents.lines().map(|l| Sensor::new(l)).collect();
    let maxi: usize = 4000000;
    for y in 0..maxi + 1 {
        let mut row = Row::new();
        let mut beacons = HashSet::new();
        for sensor in &sensors {
            if sensor.beacon.y == y as isize {
                beacons.insert(&sensor.beacon);
            }
            if let Some(segment) = sensor.get_segment(y as isize) {
                row.add(segment);
            }
        }
        row.trim(maxi as isize);
        if row.num_occupied() <= maxi {
            println!("Part 2: {}", (row.segments[0].1 + 1) * 4000000 + y as isize);
            break;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn prepend() {
        let mut row = Row::new();
        row.add((10, 20));
        row.add((0, 5));
        assert_eq!(row.segments, vec![(0, 5), (10, 20)]);
    }

    #[test]
    fn append() {
        let mut row = Row::new();
        row.add((0, 5));
        row.add((10, 20));
        assert_eq!(row.segments, vec![(0, 5), (10, 20)]);
    }

    #[test]
    fn insert() {
        let mut row = Row::new();
        row.add((0, 10));
        row.add((20, 30));
        row.add((12, 17));
        assert_eq!(row.segments, vec![(0, 10), (12, 17), (20, 30)]);
    }

    #[test]
    fn merge_front() {
        let mut row = Row::new();
        row.add((8, 20));
        row.add((0, 10));
        assert_eq!(row.segments, vec![(0, 20)]);
    }

    #[test]
    fn merge_back() {
        let mut row = Row::new();
        row.add((0, 10));
        row.add((8, 20));
        assert_eq!(row.segments, vec![(0, 20)]);
    }

    #[test]
    fn touch() {
        let mut row = Row::new();
        row.add((0, 10));
        row.add((11, 20));
        assert_eq!(row.segments, vec![(0, 20)]);
    }

    #[test]
    fn touch_front() {
        let mut row = Row::new();
        row.add((11, 20));
        row.add((0, 10));
        assert_eq!(row.segments, vec![(0, 20)]);
    }

    #[test]
    fn consume_touch_front() {
        let mut row = Row::new();
        row.add((6, 10));
        row.add((12, 14));
        row.add((-8, 12));
        assert_eq!(row.segments, vec![(-8, 14)]);
    }

    #[test]
    fn consumed() {
        let mut row = Row::new();
        row.add((0, 20));
        row.add((5, 10));
        assert_eq!(row.segments, vec![(0, 20)]);
    }

    #[test]
    fn consume() {
        let mut row = Row::new();
        row.add((5, 10));
        row.add((0, 20));
        assert_eq!(row.segments, vec![(0, 20)]);
    }

    #[test]
    fn consume_many() {
        let mut row = Row::new();
        row.add((5, 10));
        row.add((15, 20));
        row.add((25, 30));
        row.add((0, 40));
        assert_eq!(row.segments, vec![(0, 40)]);
    }

    #[test]
    fn merge_consume() {
        let mut row = Row::new();
        row.add((0, 10));
        row.add((15, 20));
        row.add((5, 30));
        assert_eq!(row.segments, vec![(0, 30)]);
    }

    #[test]
    fn consume_merge() {
        let mut row = Row::new();
        row.add((5, 10));
        row.add((15, 30));
        row.add((0, 20));
        assert_eq!(row.segments, vec![(0, 30)]);
    }
}
