use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn has_dupes(s: &str) -> bool {
    let chars: Vec<char> = s.chars().collect();
    for i in 0..chars.len() - 1 {
        for j in i + 1..chars.len() {
            if chars[i] == chars[j] {
                return true;
            }
        }
    }
    return false;
}

fn dupes(s: &str, l: usize) -> usize {
    for i in 0..s.len() - (l - 1) {
        if !has_dupes(&s[i..i + l]) {
            return i + l;
        }
    }
    return 0;
}

fn part1(contents: &str) {
    let line: &str = contents.lines().next().unwrap();
    println!("Part 1: {}", dupes(line, 4));
}

fn part2(contents: &str) {
    let line: &str = contents.lines().next().unwrap();
    println!("Part 2: {}", dupes(line, 14));
}
