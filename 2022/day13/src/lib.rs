use std::cmp::Ordering;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn to_vec(packet: &str) -> Vec<String> {
    if !packet.starts_with('[') {
        return vec![packet.to_string()];
    }

    let mut result: Vec<String> = vec![];
    let cs: Vec<char> = packet[1..packet.len() - 1].chars().collect();
    let mut i = 0;
    while i < cs.len() {
        if cs[i] == '[' {
            let mut lefts = 1;
            let mut j = 1;
            while lefts > 0 {
                if cs[i + j] == '[' {
                    lefts += 1;
                } else if cs[i + j] == ']' {
                    lefts -= 1;
                }
                j += 1;
            }
            result.push(String::from_iter(&cs[i..i + j]));
            i = i + j;
        } else if cs[i] == ']' {
            panic!("Found unmatched ']': {}", packet);
        } else {
            let mut j = 0;
            while i + j < cs.len() && cs[i + j] != ',' && cs[i + j] != ']' {
                j += 1;
            }
            result.push(String::from_iter(&cs[i..i + j]));
            i = i + j;
        }
        i += 1;
    }
    return result;
}

fn is_int(packet: &str) -> bool {
    return packet.chars().collect::<Vec<char>>()[0] != '[';
}

fn compare(p1: &str, p2: &str) -> Ordering {
    if is_int(p1) && is_int(p2) {
        let i1 = p1.parse::<usize>().unwrap();
        let i2 = p2.parse::<usize>().unwrap();
        if i1 == i2 {
            return Ordering::Equal;
        } else if i1 < i2 {
            return Ordering::Less;
        } else {
            return Ordering::Greater;
        }
    }
    if is_int(p1) {
        return compare(&("[".to_owned() + p1 + "]"), p2);
    }
    if is_int(p2) {
        return compare(p1, &("[".to_owned() + p2 + "]"));
    }
    let mut i = 0;
    let v1 = to_vec(p1);
    let v2 = to_vec(p2);
    while i < v1.len() && i < v2.len() {
        let comp = compare(&v1[i], &v2[i]);
        if comp != Ordering::Equal {
            return comp;
        }
        i += 1;
    }
    if v1.len() < v2.len() {
        return Ordering::Less;
    } else if v1.len() > v2.len() {
        return Ordering::Greater;
    }
    Ordering::Equal
}

fn part1(contents: &str) {
    let packets: Vec<&str> = contents.lines().filter(|l| !l.is_empty()).collect();
    let mut indices = 0;
    for pair in 0..packets.len() / 2 {
        let comp = compare(packets[2 * pair], packets[2 * pair + 1]);
        if comp == Ordering::Less {
            indices += pair + 1
        }
    }

    println!("Part 1: {}", indices);
}

fn part2(contents: &str) {
    let mut packets: Vec<&str> = contents.lines().filter(|l| !l.is_empty()).collect();
    let divider1 = "[[2]]";
    let divider2 = "[[6]]";
    packets.push(divider1);
    packets.push(divider2);
    packets.sort_by(|a, b| compare(a, b));
    if let Some(i1) = packets.iter().position(|&p| p == divider1) {
        if let Some(i2) = packets.iter().position(|&p| p == divider2) {
            println!("Part 2: {}", (i1 + 1) * (i2 + 1));
        }
    }
}
