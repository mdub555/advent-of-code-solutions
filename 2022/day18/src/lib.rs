use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Voxel {
    x: usize,
    y: usize,
    z: usize,
    top: bool,
    bottom: bool,
    left: bool,
    right: bool,
    front: bool,
    back: bool,
}

impl Voxel {
    fn new(line: &str) -> Voxel {
        let dims: Vec<usize> = line.split(",").filter_map(|l| l.parse().ok()).collect();
        Voxel {
            x: dims[0],
            y: dims[1],
            z: dims[2],
            top: false,
            bottom: false,
            left: false,
            right: false,
            front: false,
            back: false,
        }
    }
}

#[derive(Debug)]
struct Cuboid {
    voxels: Vec<Voxel>,
}

impl Cuboid {
    fn new(voxels: Vec<Voxel>) -> Cuboid {
        Cuboid { voxels }
    }

    fn surface_area(&mut self) -> usize {
        let mut surface = 0;
        // z is sorted last, go through all x, y.
        self.voxels.sort_unstable_by_key(|k| (k.x, k.y, k.z));
        for i in 0..self.voxels.len() {
            // bottom
            if i <= 0
                || (self.voxels[i - 1].x, self.voxels[i - 1].y)
                    != (self.voxels[i].x, self.voxels[i].y)
                || (self.voxels[i - 1].z < self.voxels[i].z - 1)
            {
                self.voxels[i].bottom = true;
                surface += 1;
            }

            // top
            if i >= self.voxels.len() - 1
                || (self.voxels[i].x, self.voxels[i].y)
                    != (self.voxels[i + 1].x, self.voxels[i + 1].y)
                || (self.voxels[i].z + 1 < self.voxels[i + 1].z)
            {
                self.voxels[i].top = true;
                surface += 1;
            }
        }

        // y is sorted last, go through all x, z.
        self.voxels.sort_unstable_by_key(|k| (k.x, k.z, k.y));
        for i in 0..self.voxels.len() {
            // back
            if i <= 0
                || (self.voxels[i - 1].x, self.voxels[i - 1].z)
                    != (self.voxels[i].x, self.voxels[i].z)
                || (self.voxels[i - 1].y < self.voxels[i].y - 1)
            {
                self.voxels[i].back = true;
                surface += 1;
            }

            // front
            if i >= self.voxels.len() - 1
                || (self.voxels[i].x, self.voxels[i].z)
                    != (self.voxels[i + 1].x, self.voxels[i + 1].z)
                || (self.voxels[i].y + 1 < self.voxels[i + 1].y)
            {
                self.voxels[i].front = true;
                surface += 1;
            }
        }

        // x is sorted last, go through all y, z.
        self.voxels.sort_unstable_by_key(|k| (k.y, k.z, k.x));
        for i in 0..self.voxels.len() {
            // left
            if i <= 0
                || (self.voxels[i - 1].y, self.voxels[i - 1].z)
                    != (self.voxels[i].y, self.voxels[i].z)
                || (self.voxels[i - 1].x < self.voxels[i].x - 1)
            {
                self.voxels[i].left = true;
                surface += 1;
            }

            // right
            if i >= self.voxels.len() - 1
                || (self.voxels[i].y, self.voxels[i].z)
                    != (self.voxels[i + 1].y, self.voxels[i + 1].z)
                || (self.voxels[i].x + 1 < self.voxels[i + 1].x)
            {
                self.voxels[i].right = true;
                surface += 1;
            }
        }
        return surface;
    }
}

fn part1(contents: &str) {
    let voxels: Vec<Voxel> = contents.lines().map(Voxel::new).collect();
    let mut cuboid = Cuboid::new(voxels);
    println!("Part 1: {}", cuboid.surface_area());
}

fn part2(contents: &str) {}
