use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn part1(contents: &str) {
    let mut calories: Vec<u32> = Vec::new();
    let mut current: u32 = 0;
    for line in contents.lines() {
        if line.trim().is_empty() {
            calories.push(current);
            current = 0;
            continue;
        }
        current += line.parse::<u32>().unwrap();
    }
    calories.sort();
    println!("{:?}", calories[calories.len() - 1]);
}

fn part2(contents: &str) {
    let mut calories: Vec<u32> = Vec::new();
    let mut current: u32 = 0;
    for line in contents.lines() {
        if line.trim().is_empty() {
            calories.push(current);
            current = 0;
            continue;
        }
        current += line.parse::<u32>().unwrap();
    }
    calories.push(current);
    calories.sort();
    println!("{:?}", calories[calories.len() - 3..].iter().sum::<u32>());
}
