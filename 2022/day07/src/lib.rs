use std::cell::RefCell;
use std::error::Error;
use std::fs;
use std::rc::Rc;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct File {
    label: String,
    size: Option<usize>,
    children: Vec<Rc<RefCell<File>>>,
}

impl File {
    fn new(label: &str) -> File {
        File {
            label: label.to_string(),
            size: None,
            children: vec![],
        }
    }
}

fn get_size(file: &mut File) -> usize {
    if let Some(s) = file.size {
        return s;
    }
    let mut total: usize = 0;
    let children = &file.children.clone();
    for c in children {
        total += get_size(&mut c.borrow_mut());
    }
    file.size = Some(total);
    return total;
}

fn print(file: &File, depth: usize) {
    for _ in 0..depth {
        print!("  ");
    }
    println!("{}: {:?}", file.label, &file.size);
    for c in &file.children {
        print(&c.borrow(), depth + 1);
    }
}

fn sum_smaller(file: &File, size: usize) -> usize {
    if file.children.is_empty() {
        return 0;
    }
    let mut sum = 0;
    for c in &file.children {
        sum += sum_smaller(&c.borrow(), size);
    }
    if let Some(s) = file.size {
        if s <= size {
            sum += s;
        }
    }
    return sum;
}

fn find_just_larger(file: &File, size: usize) -> Option<usize> {
    if file.children.is_empty() {
        return None;
    }

    if let Some(s) = file.size {
        if s < size {
            return None;
        }
    }
    for c in &file.children {
        if let Some(s) = find_just_larger(&c.borrow(), size) {
            return Some(s);
        }
    }
    return file.size;
}

fn parse_filesystem(contents: &str) -> Rc<RefCell<File>> {
    let root = Rc::new(RefCell::new(File::new("/")));
    let mut stack: Vec<Rc<RefCell<File>>> = vec![Rc::clone(&root)];
    for line in contents.lines() {
        let tokens: Vec<&str> = line.split(' ').collect();
        match tokens[0] {
            "$" => match tokens[1] {
                "cd" => match tokens[2] {
                    "/" => (),
                    ".." => {
                        stack.pop();
                    }
                    _ => {
                        let tmp = Rc::clone(
                            stack
                                .last()
                                .unwrap()
                                .borrow()
                                .children
                                .iter()
                                .find(|&c| c.borrow().label == tokens[2])
                                .unwrap(),
                        );
                        stack.push(tmp);
                    }
                },
                _ => (),
            },
            _ => {
                let file = Rc::new(RefCell::new(File::new(tokens[1])));
                match tokens[0] {
                    "dir" => (),
                    _ => file.borrow_mut().size = Some(tokens[0].parse().unwrap()),
                }
                stack.last().unwrap().borrow_mut().children.push(file);
            }
        }
    }
    return root;
}

fn part1(contents: &str) {
    let root = parse_filesystem(contents);
    get_size(&mut root.borrow_mut());
    println!("Part 1: {}", sum_smaller(&root.borrow(), 100000));
}

fn part2(contents: &str) {
    let root = parse_filesystem(contents);
    get_size(&mut root.borrow_mut());
    let delete_size = 30000000 - (70000000 - root.borrow().size.unwrap());
    println!(
        "Part 2: {}",
        find_just_larger(&root.borrow(), delete_size).unwrap()
    );
}
