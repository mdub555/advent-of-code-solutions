use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn parse(contents: &str) -> Vec<Vec<usize>> {
    let mut grid: Vec<Vec<usize>> = vec![];
    let mut row = 0;
    for line in contents.lines() {
        grid.push(vec![]);
        for c in line.chars() {
            grid[row].push(c.to_string().parse().unwrap());
        }
        row += 1;
    }
    return grid;
}

#[derive(Debug)]
struct View {
    t: ViewType,
    dist: usize,
}

#[derive(Debug)]
enum ViewType {
    Edge,
    Tree,
}

fn view_left(grid: &Vec<Vec<usize>>, row: usize, col: usize) -> View {
    let height = grid[row][col];
    for c in (0..col).rev() {
        if grid[row][c] >= height {
            return View {
                t: ViewType::Tree,
                dist: col - c,
            };
        }
    }
    View {
        t: ViewType::Edge,
        dist: col,
    }
}

fn view_right(grid: &Vec<Vec<usize>>, row: usize, col: usize) -> View {
    let height = grid[row][col];
    for c in col + 1..grid[row].len() {
        if grid[row][c] >= height {
            return View {
                t: ViewType::Tree,
                dist: c - col,
            };
        }
    }
    View {
        t: ViewType::Edge,
        dist: grid[row].len() - 1 - col,
    }
}

fn view_above(grid: &Vec<Vec<usize>>, row: usize, col: usize) -> View {
    let height = grid[row][col];
    for r in (0..row).rev() {
        if grid[r][col] >= height {
            return View {
                t: ViewType::Tree,
                dist: row - r,
            };
        }
    }
    View {
        t: ViewType::Edge,
        dist: row,
    }
}

fn view_below(grid: &Vec<Vec<usize>>, row: usize, col: usize) -> View {
    let height = grid[row][col];
    for r in row + 1..grid.len() {
        if grid[r][col] >= height {
            return View {
                t: ViewType::Tree,
                dist: r - row,
            };
        }
    }
    View {
        t: ViewType::Edge,
        dist: grid.len() - 1 - row,
    }
}

fn visible(grid: &Vec<Vec<usize>>, row: usize, col: usize) -> bool {
    match view_right(grid, row, col).t {
        ViewType::Edge => return true,
        ViewType::Tree => (),
    }
    match view_left(grid, row, col).t {
        ViewType::Edge => return true,
        ViewType::Tree => (),
    }
    match view_above(grid, row, col).t {
        ViewType::Edge => return true,
        ViewType::Tree => (),
    }
    match view_below(grid, row, col).t {
        ViewType::Edge => return true,
        ViewType::Tree => (),
    }
    return false;
}

fn scenic_score(grid: &Vec<Vec<usize>>, row: usize, col: usize) -> usize {
    return view_right(grid, row, col).dist
        * view_left(grid, row, col).dist
        * view_above(grid, row, col).dist
        * view_below(grid, row, col).dist;
}

fn part1(contents: &str) {
    let grid = parse(contents);
    let mut num_visible = 0;
    for r in 0..grid.len() {
        for c in 0..grid[r].len() {
            if visible(&grid, r, c) {
                num_visible += 1;
            }
        }
    }
    println!("Part 1: {}", num_visible);
}

fn part2(contents: &str) {
    let grid = parse(contents);
    let mut max_score = 0;
    let mut best_col = 0;
    let mut best_row = 0;
    for r in 0..grid.len() {
        for c in 0..grid[r].len() {
            let score = scenic_score(&grid, r, c);
            if score > max_score {
                max_score = score;
                best_col = c;
                best_row = r;
            }
        }
    }
    println!(
        "Part 2: score={}, row={}, col={}",
        max_score, best_row, best_col
    );
}
