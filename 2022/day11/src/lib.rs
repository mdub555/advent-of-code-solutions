use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Monkey {
    items: Vec<usize>,
    op: fn(usize) -> usize,
    test: fn(usize) -> usize,
    inspections: usize,
}

struct Throw {
    item: usize,
    monkey: usize,
}

impl Monkey {
    fn new(items: Vec<usize>, op: fn(usize) -> usize, test: fn(usize) -> usize) -> Monkey {
        Monkey {
            items,
            op,
            test,
            inspections: 0,
        }
    }

    fn throw(&mut self) -> Throw {
        self.inspections += 1;
        let item: usize = self.items.remove(0);
        // This line is manually changed for the different parts.
        // For part 1, it looks like:
        // let val: usize = (self.op)(item) / 3;
        // For part 2, the worry numbers get too big, so I had to
        // redice the value of these numbers by moding by the product
        // of each of the test values. The "/ 3" part is also removed.
        // The modulus for the test input is 96577.
        let val: usize = (self.op)(item) % 9699690;
        return Throw {
            item: val,
            monkey: (self.test)(val),
        };
    }
}

fn round(monkeys: &mut Vec<Monkey>) {
    for m in 0..monkeys.len() {
        while !monkeys[m].items.is_empty() {
            let throw = monkeys[m].throw();
            monkeys[throw.monkey].items.push(throw.item);
        }
    }
}

fn init_test() -> Vec<Monkey> {
    let monkey0 = Monkey::new(
        vec![79, 98],
        |x| -> usize { x * 19 },
        |x| -> usize { return if x % 23 == 0 { 2 } else { 3 } },
    );
    let monkey1 = Monkey::new(
        vec![54, 65, 75, 74],
        |x| -> usize { x + 6 },
        |x| -> usize { return if x % 19 == 0 { 2 } else { 0 } },
    );
    let monkey2 = Monkey::new(
        vec![79, 60, 97],
        |x| -> usize { x * x },
        |x| -> usize { return if x % 13 == 0 { 1 } else { 3 } },
    );
    let monkey3 = Monkey::new(
        vec![74],
        |x| -> usize { x + 3 },
        |x| -> usize { return if x % 17 == 0 { 0 } else { 1 } },
    );
    vec![monkey0, monkey1, monkey2, monkey3]
}

fn init_prod() -> Vec<Monkey> {
    let monkey0 = Monkey::new(
        vec![85, 79, 63, 72],
        |x| -> usize { x * 17 },
        |x| -> usize { return if x % 2 == 0 { 2 } else { 6 } },
    );
    let monkey1 = Monkey::new(
        vec![53, 94, 65, 81, 93, 73, 57, 92],
        |x| -> usize { x * x },
        |x| -> usize { return if x % 7 == 0 { 0 } else { 2 } },
    );
    let monkey2 = Monkey::new(
        vec![62, 63],
        |x| -> usize { x + 7 },
        |x| -> usize { return if x % 13 == 0 { 7 } else { 6 } },
    );
    let monkey3 = Monkey::new(
        vec![57, 92, 56],
        |x| -> usize { x + 4 },
        |x| -> usize { return if x % 5 == 0 { 4 } else { 5 } },
    );
    let monkey4 = Monkey::new(
        vec![67],
        |x| -> usize { x + 5 },
        |x| -> usize { return if x % 3 == 0 { 1 } else { 5 } },
    );
    let monkey5 = Monkey::new(
        vec![85, 56, 66, 72, 57, 99],
        |x| -> usize { x + 6 },
        |x| -> usize { return if x % 19 == 0 { 1 } else { 0 } },
    );
    let monkey6 = Monkey::new(
        vec![86, 65, 98, 97, 69],
        |x| -> usize { x * 13 },
        |x| -> usize { return if x % 11 == 0 { 3 } else { 7 } },
    );
    let monkey7 = Monkey::new(
        vec![87, 68, 92, 66, 91, 50, 68],
        |x| -> usize { x + 2 },
        |x| -> usize { return if x % 17 == 0 { 4 } else { 3 } },
    );
    vec![
        monkey0, monkey1, monkey2, monkey3, monkey4, monkey5, monkey6, monkey7,
    ]
}

fn part1(_contents: &str) {
    let mut monkeys = init_test();
    for _ in 0..20 {
        round(&mut monkeys);
    }
    let mut inspections = vec![];
    for m in monkeys {
        inspections.push(m.inspections);
    }
    inspections.sort();
    inspections.reverse();
    println!("{}", inspections[0] * inspections[1]);
}

fn part2(_contents: &str) {
    let mut monkeys = init_prod();
    for _ in 0..10000 {
        round(&mut monkeys);
    }
    let mut inspections = vec![];
    for m in monkeys {
        inspections.push(m.inspections);
    }
    inspections.sort();
    inspections.reverse();
    println!("{}", inspections[0] * inspections[1]);
}
