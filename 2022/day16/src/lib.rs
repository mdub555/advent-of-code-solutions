use priority_queue::PriorityQueue;
use regex::Regex;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::error::Error;
use std::fs;
use std::rc::Rc;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Node {
    label: String,
    flow: usize,
    neighbors: Vec<String>,
    value: usize,
    time: usize,
}

impl Node {
    fn new(label: &str, flow: usize, neighbors: Vec<String>) -> Node {
        Node {
            label: label.to_string(),
            flow,
            neighbors,
            value: 0,
            time: 0,
        }
    }

    fn from(line: &str) -> Node {
        let re =
            Regex::new(r"^Valve ([A-Z]+) has flow rate=(\d+); tunnels? leads? to valves? (.*)$")
                .unwrap();
        let caps = re.captures(line).unwrap();
        let neighbors: Vec<String> = caps
            .get(3)
            .unwrap()
            .as_str()
            .to_string()
            .split(", ")
            .map(str::to_string)
            .collect();
        Node {
            label: caps.get(1).unwrap().as_str().to_string(),
            flow: caps.get(2).unwrap().as_str().parse().unwrap(),
            neighbors: neighbors,
            value: 0,
            time: 0,
        }
    }
}

fn dijkstras(graph: &HashMap<String, RefCell<Node>>, start: &str) {
    let mut q = PriorityQueue::new();
    for (label, node) in graph.iter() {
        q.push(label.to_string(), node.borrow().value);
    }
    q.remove(&String::from(start));

    while !q.is_empty() {
        let node = graph.get(&q.pop().unwrap().0).unwrap().borrow();
        for neighbor in &node.neighbors {
            if q.get(neighbor) == None {
                continue;
            }
            let val = node.value + graph.get(neighbor).unwrap().borrow().flow;
            if val > graph.get(neighbor).unwrap().borrow().value {
                graph.get(neighbor).unwrap().borrow_mut().value = val;
                q.change_priority(neighbor, val);
            }
        }
    }
}

fn part1(contents: &str) {
    let mut nodes: HashMap<String, RefCell<Node>> = HashMap::new();
    for line in contents.lines() {
        let mut n = Node::from(line);
        if n.flow != 0 {
            let label: String = n.label.to_string() + "2";
            nodes.insert(
                String::from(&label),
                RefCell::new(Node::new(&label, n.flow, n.neighbors.to_vec())),
            );
            n.flow = 0;
            n.neighbors.push(label);
        }
        nodes.insert(String::from(&n.label), RefCell::new(n));
    }

    dijkstras(&nodes, "AA");
    println!("{:?}", nodes);
}

fn part2(contents: &str) {}
