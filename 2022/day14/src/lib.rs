use std::cmp::{max, min};
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

struct Grid {
    grid: Vec<Vec<char>>,
    minrow: usize,
    maxrow: usize,
    mincol: usize,
    maxcol: usize,
    num_sand: usize,
}

impl Grid {
    fn new(rocks: &Vec<Vec<(usize, usize)>>) -> Grid {
        // Get the bounds of the rocks to initiate the grid.
        let mut mincol: usize = 1000000;
        let mut maxcol: usize = 0;
        let mut maxrow: usize = 0;
        let minrow: usize = 0;
        for rock in rocks {
            for line in rock {
                maxcol = max(line.0, maxcol);
                mincol = min(line.0, mincol);
                maxrow = max(line.1, maxrow);
            }
        }

        // Initiate the grid.
        let mut grid = vec![];
        for _ in minrow..maxrow + 1 {
            let mut row = vec![];
            for _ in mincol..maxcol + 1 {
                row.push('.');
            }
            grid.push(row);
        }

        // Add the rocks to the grid.
        for rock in rocks {
            for i in 0..rock.len() - 1 {
                if rock[i].0 == rock[i + 1].0 {
                    // Vertical line
                    let start = min(rock[i].1, rock[i + 1].1);
                    let end = max(rock[i].1, rock[i + 1].1);
                    for y in start..end + 1 {
                        grid[y - minrow][rock[i].0 - mincol] = '#';
                    }
                } else {
                    // Horizontal line
                    let start = min(rock[i].0, rock[i + 1].0);
                    let end = max(rock[i].0, rock[i + 1].0);
                    for x in start..end + 1 {
                        grid[rock[i].1 - minrow][x - mincol] = '#';
                    }
                }
            }
        }

        Grid {
            grid,
            mincol,
            maxcol,
            minrow,
            maxrow,
            num_sand: 0,
        }
    }

    fn draw(&self) {
        for row in &self.grid {
            for cell in row {
                print!("{}", cell);
            }
            println!();
        }
    }

    fn drop(&mut self) -> bool {
        let mut row = 0 - self.minrow;
        let mut col = 500 - self.mincol;
        if self.grid[row][col] != '.' {
            return false;
        }
        loop {
            while row + 1 < self.grid.len() && self.grid[row + 1][col] == '.' {
                row += 1;
            }
            if row >= self.grid.len() - 1 {
                return false;
            }
            if col <= 0 {
                return false;
            }
            if self.grid[row + 1][col - 1] == '.' {
                row += 1;
                col -= 1;
                continue;
            }
            if col >= self.grid[row].len() - 1 {
                return false;
            }
            if self.grid[row + 1][col + 1] == '.' {
                row += 1;
                col += 1;
                continue;
            }
            break;
        }
        self.grid[row][col] = 'o';
        self.num_sand += 1;
        return true;
    }
}

fn to_point(coord: &str) -> (usize, usize) {
    let xy: Vec<usize> = coord.split(",").map(|x| x.parse().unwrap()).collect();
    (xy[0], xy[1])
}

fn parse(contents: &str) -> Vec<Vec<(usize, usize)>> {
    // Convert input lines to vectors of points.
    contents
        .lines()
        .map(|l| l.split(" -> ").map(|x| to_point(x)).collect())
        .collect()
}

fn part1(contents: &str) {
    let mut grid = Grid::new(&parse(contents));
    while grid.drop() {}
    println!("Part 1: {} sand", grid.num_sand);
}

fn part2(contents: &str) {
    let mut rocks = parse(contents);
    let grid = Grid::new(&rocks);
    let height = grid.maxrow - grid.minrow + 2;
    rocks.push(vec![(500 - height, height), (500 + height, height)]);
    let mut grid = Grid::new(&rocks);
    while grid.drop() {}
    println!("Part 2: {} sand", grid.num_sand);
}
