use std::cmp::max;
use std::collections::HashSet;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
enum Direction {
    Left,
    Right,
}

impl Direction {
    fn new(dir: char) -> Direction {
        match dir {
            '>' => Direction::Right,
            '<' => Direction::Left,
            _ => panic!("Unknown direction: {}", dir),
        }
    }
}

#[derive(Debug)]
struct Piece {
    shape: Vec<Vec<bool>>,
    row: usize,
    col: usize,
}

impl Piece {
    fn new(shape: &Vec<Vec<bool>>) -> Piece {
        Piece {
            shape: shape.to_vec(),
            row: 0,
            col: 2,
        }
    }

    fn width(&self) -> usize {
        return self.shape[0].len();
    }

    fn height(&self) -> usize {
        return self.shape.len();
    }

    fn contains(&self, r: usize, c: usize) -> bool {
        return r <= self.row
            && r as isize > self.row as isize - self.height() as isize
            && c >= self.col
            && c < self.col + self.width();
    }
}

fn get_piece(i: usize) -> Piece {
    let p0 = vec![vec![true, true, true, true]];
    let p1 = vec![
        vec![false, true, false],
        vec![true, true, true],
        vec![false, true, false],
    ];
    let p2 = vec![
        vec![true, true, true],
        vec![false, false, true],
        vec![false, false, true],
    ];
    let p3 = vec![vec![true], vec![true], vec![true], vec![true]];
    let p4 = vec![vec![true, true], vec![true, true]];
    match i {
        0 => Piece::new(&p0),
        1 => Piece::new(&p1),
        2 => Piece::new(&p2),
        3 => Piece::new(&p3),
        4 => Piece::new(&p4),
        _ => panic!("No piece: {}", i),
    }
}

fn add(grid: &mut Vec<Vec<bool>>, piece: &Piece) {
    for r in 0..piece.height() {
        for c in 0..piece.width() {
            if piece.shape[r][c] {
                grid[r + piece.row][c + piece.col] = true;
            }
        }
    }
}

fn drop(grid: &mut Vec<Vec<bool>>, piece: &mut Piece) -> bool {
    if piece.row <= 0 {
        add(grid, piece);
        return false;
    }
    for row in 0..piece.height() {
        for col in 0..piece.width() {
            if piece.shape[row][col] && grid[piece.row + row - 1][piece.col + col] {
                add(grid, piece);
                return false;
            }
        }
    }
    piece.row -= 1;
    return true;
}

fn blow(grid: &Vec<Vec<bool>>, piece: &mut Piece, dir: &Direction) {
    match dir {
        Direction::Right => {
            if piece.col + piece.width() >= grid[0].len() {
                return;
            }
            for row in 0..piece.height() {
                for col in 0..piece.width() {
                    if piece.shape[row][col] && grid[piece.row + row][piece.col + col + 1] {
                        return;
                    }
                }
            }
            piece.col += 1;
        }
        Direction::Left => {
            if piece.col == 0 {
                return;
            }
            for row in 0..piece.height() {
                for col in 0..piece.width() {
                    if piece.shape[row][col] && grid[piece.row + row][piece.col + col - 1] {
                        return;
                    }
                }
            }
            piece.col -= 1;
        }
    }
}

fn spawn(grid: &mut Vec<Vec<bool>>, piece: usize) -> Piece {
    let mut p = get_piece(piece);
    p.row = 4;
    'outer: for r in (0..grid.len()).rev() {
        for c in 0..grid[r].len() {
            if grid[r][c] {
                p.row = r + 5;
                break 'outer;
            }
        }
    }

    for _ in grid.len()..p.row + p.shape.len() {
        let mut r = vec![];
        for _ in 0..grid[0].len() {
            r.push(false);
        }
        grid.push(r);
    }
    return p;
}

fn draw_grid(grid: &Vec<Vec<bool>>) {
    for row in (0..grid.len()).rev() {
        print!("{:>3} ", row);
        for cell in &grid[row] {
            if *cell {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
    println!();
}

fn draw(grid: &Vec<Vec<bool>>, piece: &Piece) {
    let mut g = grid.clone();
    add(&mut g, piece);
    draw_grid(&g);
}

fn get_height(grid: &Vec<Vec<bool>>) -> usize {
    for r in (0..grid.len()).rev() {
        for c in 0..grid[0].len() {
            if grid[r][c] {
                return r + 1;
            }
        }
    }
    return 0;
}

// Trim all but the top 100 lines
fn trim(grid: &mut Vec<Vec<bool>>, remaining: usize) -> usize {
    if remaining >= grid.len() {
        return 0;
    }
    let trimmed = grid.len() - remaining;
    grid.drain(..trimmed);
    return trimmed;
}

fn part1(contents: &str) {
    let jets: Vec<Direction> = contents.chars().map(Direction::new).collect();
    let mut grid: Vec<Vec<bool>> = vec![vec![false, false, false, false, false, false, false]];
    let mut p = 0;
    let mut j = 0;
    for _ in 0..2022 {
        let mut piece = spawn(&mut grid, p);
        p += 1;
        if p % 5 == 0 {
            p = 0;
        }

        while drop(&mut grid, &mut piece) {
            blow(&grid, &mut piece, &jets[j]);
            j += 1;
            if j >= jets.len() {
                j = 0;
            }
        }
    }

    println!("Part 1: {}", get_height(&grid));
}

// Test repeats every 35 starting at 15, 50, 85, ...  pieces.
// Repeats at (piece, move) = (0, 2)
// Base height = 25, repeated height = 53.
//
// Prod repeats every 1720 starting at 185, 1905, 3625, ... pieces.
// Repeats at (piece, move = (0, 1104)
// Base height = 297, repeated height = 2626.
fn part2(contents: &str) {
    let jets: Vec<Direction> = contents.chars().map(Direction::new).collect();
    let mut grid: Vec<Vec<bool>> = vec![vec![false, false, false, false, false, false, false]];
    let mut p = 0;
    let mut j = 0;
    let mut height = 0;
    let mut base_height: Option<usize> = None;
    let mut repeat_height = 0;
    let mut base_piece: Option<usize> = None;
    let mut repeat_piece = 0;
    let total_pieces: usize = 1000000000000;
    // let total_pieces: usize = 2022;
    let mut n = 0;
    loop {
        if n % 100 == 0 {
            height += trim(&mut grid, 100);
        }
        if n >= 2000 {
            break;
        }
        n += 1;
        if (p, j) == (0, 1104) {
            match base_piece {
                None => base_piece = Some(n - 1),
                Some(x) => repeat_piece = n - x - 1,
            }
            match base_height {
                None => base_height = Some(height + get_height(&grid)),
                Some(h) => {
                    repeat_height = height + get_height(&grid) - h;
                    break;
                }
            }
        }
        let mut piece = spawn(&mut grid, p);
        p += 1;
        if p % 5 == 0 {
            p = 0;
        }

        while drop(&mut grid, &mut piece) {
            blow(&grid, &mut piece, &jets[j]);
            j += 1;
            if j >= jets.len() {
                j = 0;
            }
        }
    }

    let towers = (total_pieces - base_piece.unwrap()) / repeat_piece;
    let total_height = repeat_height * towers + base_height.unwrap();
    grid = vec![vec![false, false, false, false, false, false, false]];
    height = 0;
    for n in 0..(total_pieces - base_piece.unwrap()) % repeat_piece {
        if n % 100 == 0 {
            height += trim(&mut grid, 100);
        }
        let mut piece = spawn(&mut grid, p);
        p += 1;
        if p % 5 == 0 {
            p = 0;
        }

        while drop(&mut grid, &mut piece) {
            blow(&grid, &mut piece, &jets[j]);
            j += 1;
            if j >= jets.len() {
                j = 0;
            }
        }
    }
    height += get_height(&grid);
    println!("Part 2: {}", total_height + height);
}
