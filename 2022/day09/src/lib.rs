use std::cmp::{max, min};
use std::collections::HashSet;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
struct Pos {
    x: isize,
    y: isize,
}

impl Pos {
    fn new(x: isize, y: isize) -> Pos {
        Pos { x, y }
    }
}

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn move_head(head: &mut Pos, dir: &Direction) {
    match dir {
        Direction::Up => head.y -= 1,
        Direction::Down => head.y += 1,
        Direction::Right => head.x += 1,
        Direction::Left => head.x -= 1,
    }
}

fn react(rope: &mut Vec<Pos>) {
    for i in 1..rope.len() {
        let dx = rope[i - 1].x - rope[i].x;
        let dy = rope[i - 1].y - rope[i].y;
        if dx.abs() <= 1 && dy.abs() <= 1 {
            return;
        }
        if dx > 0 {
            rope[i].x += 1;
        }
        if dx < 0 {
            rope[i].x -= 1;
        }
        if dy > 0 {
            rope[i].y += 1;
        }
        if dy < 0 {
            rope[i].y -= 1;
        }
    }
}

fn apply(rope: &mut Vec<Pos>, tails: &mut HashSet<Pos>, motion: &str) {
    let tokens: Vec<&str> = motion.split(' ').collect();
    let dir = match tokens[0] {
        "U" => Direction::Up,
        "D" => Direction::Down,
        "L" => Direction::Left,
        "R" => Direction::Right,
        _ => panic!("Unknown direction: {}", tokens[0]),
    };
    let dist: usize = tokens[1].parse().unwrap();
    for _ in 0..dist {
        move_head(&mut rope[0], &dir);
        react(rope);
        tails.insert(rope[rope.len() - 1].clone());
    }
}

// A bounding box of two points, the top-left and bottom-right.
struct Bounds {
    tl: Pos,
    br: Pos,
}

// Get the bounding box of the rope.
fn get_bounds(rope: &Vec<Pos>) -> Bounds {
    let mut left = 100000;
    let mut right = -100000;
    let mut top = 100000;
    let mut bottom = -100000;
    for segment in rope {
        right = max(right, segment.x);
        left = min(left, segment.x);
        top = min(top, segment.y);
        bottom = max(bottom, segment.y);
    }
    return Bounds {
        tl: Pos::new(left, top),
        br: Pos::new(right, bottom),
    };
}

// Draw the rope on the grid. Useful for debugging.
fn draw(rope: &Vec<Pos>) {
    let bounds = get_bounds(rope);
    for row in bounds.tl.y..bounds.br.y + 1 {
        for col in bounds.tl.x..bounds.br.x + 1 {
            match rope.iter().position(|p| p.x == col && p.y == row) {
                None => print!("."),
                Some(i) => print!("{}", i),
            }
        }
        println!();
    }
    println!();
}

fn part1(contents: &str) {
    let mut rope = vec![Pos::new(0, 0), Pos::new(0, 0)];
    let mut tails: HashSet<Pos> = HashSet::new();
    for line in contents.lines() {
        apply(&mut rope, &mut tails, line);
    }
    println!("Part 1: {} spots", tails.len());
}

fn part2(contents: &str) {
    let mut rope = vec![];
    for _ in 0..10 {
        rope.push(Pos::new(0, 0));
    }
    let mut tails: HashSet<Pos> = HashSet::new();
    for line in contents.lines() {
        apply(&mut rope, &mut tails, line);
    }
    println!("Part 2: {} spots", tails.len());
}
