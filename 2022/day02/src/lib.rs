use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

#[derive(Debug)]
struct Round {
    player1: Choice,
    player2: Choice,
}

impl Round {
    fn parse1(line: &str) -> Self {
        let choices: Vec<&str> = line.split(" ").collect();
        let player1 = match choices[1] {
            "X" => Choice::Rock,
            "Y" => Choice::Paper,
            "Z" => Choice::Scissors,
            _ => panic!("Couldn't parse the line"),
        };
        let player2 = Round::parse_player2(choices[0]);
        return Round { player1, player2 };
    }

    fn parse2(line: &str) -> Self {
        let choices: Vec<&str> = line.split(" ").collect();
        let player2 = Round::parse_player2(choices[0]);
        let outcome = match choices[1] {
            "X" => Outcome::Loss,
            "Y" => Outcome::Tie,
            "Z" => Outcome::Win,
            _ => panic!("Couldn't parse the line"),
        };
        return Round {
            player1: Round::to_get_outcome(&player2, &outcome),
            player2,
        };
    }

    fn score(&self) -> u32 {
        let mut score: u32 = value(&self.player1);
        match Round::outcome(&self.player1, &self.player2) {
            Outcome::Loss => score += 0,
            Outcome::Tie => score += 3,
            Outcome::Win => score += 6,
        }
        return score;
    }

    fn outcome(yours: &Choice, theirs: &Choice) -> Outcome {
        match yours {
            Choice::Rock => match theirs {
                Choice::Rock => Outcome::Tie,
                Choice::Paper => Outcome::Loss,
                Choice::Scissors => Outcome::Win,
            },
            Choice::Paper => match theirs {
                Choice::Rock => Outcome::Win,
                Choice::Paper => Outcome::Tie,
                Choice::Scissors => Outcome::Loss,
            },
            Choice::Scissors => match theirs {
                Choice::Rock => Outcome::Loss,
                Choice::Paper => Outcome::Win,
                Choice::Scissors => Outcome::Tie,
            },
        }
    }

    fn to_get_outcome(theirs: &Choice, outcome: &Outcome) -> Choice {
        match outcome {
            Outcome::Win => match theirs {
                Choice::Rock => Choice::Paper,
                Choice::Paper => Choice::Scissors,
                Choice::Scissors => Choice::Rock,
            },
            Outcome::Tie => theirs.clone(),
            Outcome::Loss => match theirs {
                Choice::Rock => Choice::Scissors,
                Choice::Paper => Choice::Rock,
                Choice::Scissors => Choice::Paper,
            },
        }
    }

    fn parse_player2(choice: &str) -> Choice {
        match choice {
            "A" => Choice::Rock,
            "B" => Choice::Paper,
            "C" => Choice::Scissors,
            _ => panic!("Couldn't parse the line"),
        }
    }
}

#[derive(Debug, Clone)]
enum Choice {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug)]
enum Outcome {
    Win,
    Tie,
    Loss,
}

fn value(choice: &Choice) -> u32 {
    match choice {
        Choice::Rock => 1,
        Choice::Paper => 2,
        Choice::Scissors => 3,
    }
}

fn part1(contents: &str) {
    let mut total: u32 = 0;
    for line in contents.lines() {
        let round = Round::parse1(line);
        total += round.score();
    }
    println!("Part 1: {:?}", total);
}

fn part2(contents: &str) {
    let mut total: u32 = 0;
    for line in contents.lines() {
        let round = Round::parse2(line);
        total += round.score();
    }
    println!("Part 2: {:?}", total);
}
