use std::collections::HashSet;
use std::error::Error;
use std::fs;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 2 {
            return Err("Specify a filename");
        }
        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;
    let contents = contents.trim();

    part1(contents);
    part2(contents);

    Ok(())
}

fn value(letter: char) -> usize {
    if letter.is_uppercase() {
        return letter as usize - 64 + 26;
    }
    return letter as usize - 96;
}

fn part1(contents: &str) {
    let mut sum = 0;
    for line in contents.lines() {
        let length = line.len();

        let mut compartment1: HashSet<char> = HashSet::with_capacity(52);
        for c in line[..length / 2].chars() {
            compartment1.insert(c);
        }

        let mut compartment2: HashSet<char> = HashSet::with_capacity(52);
        for c in line[length / 2..].chars() {
            compartment2.insert(c);
        }

        let intersection: HashSet<_> = compartment1.intersection(&compartment2).collect();
        for c in intersection {
            sum += value(*c);
        }
    }
    println!("Part 1: {}", sum);
}

fn part2(contents: &str) {
    let mut sum = 0;
    let lines: Vec<&str> = contents.lines().collect();
    for group in 0..lines.len() / 3 {
        let mut elf1: HashSet<char> = HashSet::with_capacity(52);
        for c in lines[3 * group].chars() {
            elf1.insert(c);
        }
        let mut elf2: HashSet<char> = HashSet::with_capacity(52);
        for c in lines[3 * group + 1].chars() {
            elf2.insert(c);
        }
        let mut elf3: HashSet<char> = HashSet::with_capacity(52);
        for c in lines[3 * group + 2].chars() {
            elf3.insert(c);
        }

        let intersection1: HashSet<char> = elf1.intersection(&elf2).copied().collect();
        let intersection2: HashSet<char> = intersection1.intersection(&elf3).copied().collect();
        for c in intersection2 {
            sum += value(c);
        }
    }
    println!("Part 2: {}", sum);
}
